package week9_collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CollectionFunctions {

	public static <T> List<T> mergeToList(Collection<T> coll1, Collection<T> coll2) {
		List<T> mergedList = new ArrayList<>(coll1);
		mergedList.addAll(coll2);
		return mergedList;
	}

	public static <T> Set<T> mergeToSet(Collection<T> coll1, Collection<T> coll2) {
		Set<T> mergedSet = new HashSet<>(coll1);
		mergedSet.addAll(coll2);
		return mergedSet;
	}

	public static <T> void merge(Collection<T> inputColl1, Collection<T> inputColl2, Collection<T> targetColl) {
		targetColl.addAll(inputColl1);
		targetColl.addAll(inputColl2);
	}

	public static <T extends Comparable<T>> T median(Collection<T> coll) {
		if (coll == null)
			return null;

		List<T> list = new ArrayList<>(coll);
		Collections.sort(list);
		int midIndex = 1 + (list.size()-1)/2;
		return list.get(midIndex);
	}
	
	public static <T extends Comparable<T>> Collection<T> largest(Collection<T> coll, int amount){
		if(coll.size()<amount)
			throw new IllegalArgumentException("Collection is too small");
		
		List<T> coll2 = new ArrayList<>(coll);
		Collections.sort(coll2);
		
		List<T> largest = new ArrayList<>();
		for(int i=coll2.size()-1;i>=coll2.size()-amount;i--){
			largest.add(coll2.get(i));
		}
		return largest;
	}
	
	public static <T extends Comparable<T>> Collection<T> smallest(Collection<T> coll, int amount){
		if(coll.size()<amount)
			throw new IllegalArgumentException("Collection is too small");
		
		List<T> coll2 = new ArrayList<>(coll);
		Collections.sort(coll2);
		
		return coll2.subList(0, amount);
	}
}
