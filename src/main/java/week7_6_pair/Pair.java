package week7_6_pair;

public interface Pair extends Cloneable {
	Object getFirst();
	Object getSecond();
	Pair clone() throws CloneNotSupportedException;
}
