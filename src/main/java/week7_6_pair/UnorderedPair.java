package week7_6_pair;

public class UnorderedPair extends AbstractPair {

	public UnorderedPair(Object first, Object second) {
		super(first, second);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		if(getFirst() != null && getSecond() != null){
			result = prime * result + (getFirst().hashCode()*getSecond().hashCode());
		}else{
			result = prime * result + ((getFirst() == null) ? 0 : getFirst().hashCode());
			result = prime * result + ((getSecond() == null) ? 0 : getSecond().hashCode());
		}		
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		UnorderedPair other = (UnorderedPair) obj;		
		return (this.getFirst().equals(other.getFirst()) || this.getFirst().equals(other.getSecond()))
				&& (this.getSecond().equals(other.getFirst()) || this.getSecond().equals(other.getSecond()));
	}
}
