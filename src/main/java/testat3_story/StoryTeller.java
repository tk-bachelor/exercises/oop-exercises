package testat3_story;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class StoryTeller {
	
	public static String constructStory(List<String> lines){
		Map<Integer, String> words = new TreeMap<>();
		
		lines.forEach( line -> {
			String[] splitted = line.split("=",2);
			if(splitted.length == 2){
				words.put(Integer.parseInt(splitted[0]), splitted[1]);
			}
		});
		return makeText(words.values());
	}
	
	private static String makeText(Collection<String> words){
		StringBuilder textBuilder = new StringBuilder();
		for(String word : words){
			textBuilder.append(word + " ");
		}
		return textBuilder.toString();
	}
}
