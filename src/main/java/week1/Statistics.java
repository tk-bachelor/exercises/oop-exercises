package week1;

import java.util.Random;

/**
 * @author Keerthikan Week 1 - Exercise 4
 * 
 *         Statistics
 */
public class Statistics {
	public static void main(String[] args) {
		
		boolean testWithRandom = true;
		
		// Test with either random numbers or ordered number series (for testing purpose)
		double[] series = (testWithRandom) ? randomSeries(10000) : getOrderedNumberSeries(1, 5);
		
		analyse(series);		
	}
	
	private static void analyse(double[] series){
		int arrayLength = series.length;
		
		// calculate min and max values
		double minValue = series[0];
		double maxValue = series[0];

		for (double value : series) {
			if (value < minValue)
				minValue = value;

			if (value > maxValue) {
				maxValue = value;
			}
		}
		// print min and max
		System.out.println(String.format("min: %f max:%s", minValue, maxValue));

		// Arithmetic
		double arithmeticAvg = 0;
		for (double value : series)
			arithmeticAvg += value;
		arithmeticAvg /= arrayLength;

		// print arithmetic average
		System.out.println(String.format("Arithmetic Average: %f", arithmeticAvg));

		// Varianz
		double variance = 0;
		for (double value : series) {
			variance += Math.pow(value - arithmeticAvg, 2);
		}
		variance /= arrayLength;
		System.out.println(String.format("Variance %f", variance));

		// Abweicher
		double deviation = Math.sqrt(variance);
		System.out.println(String.format("Abweichung %f", deviation));
	}
	
	// Random number generator
	static double[] randomSeries(int amount) {
		double[] series = new double[amount];
		Random random = new Random(4711);
		for (int index = 0; index < amount; index++) {
			series[index] = random.nextDouble();
		}
		return series;
	}
	
	// Get ordered number series (eg. 1,2,3,4... n)
	static double[] getOrderedNumberSeries(int min, int max){
		double[] series = new double[max-min+1];
		
		for(int index = 0; index < series.length; index++){
			series[index] = min;
			min++;
		}
		return series;		
	}
}
