package week1;

public class ImageProcessing {
	static int[][] invert(int[][] pixels) {
		// implement image inversion (negate all pixels)
		int[][] invertedPixel = pixels;

		// row counter
		for (int row = 0; row < pixels.length; row++) {
			// column counter
			for (int column = 0; column < pixels[row].length; column++) {

				// negate the value in the cell
				invertedPixel[row][column] = -pixels[row][column];
			}
		}

		return invertedPixel;
	}

	// rotate the image by 90� clockwise
	static int[][] rotate(int[][] pixels) {
		// Calculate old size
		int oldWidth = pixels[0].length;
		int oldHeight = pixels.length;

		// swap width and height
		int newHeight = oldWidth;
		int newWidth = oldHeight;

		// define new holder
		int[][] rotatedPixels = new int[newHeight][newWidth];

		// old row
		for (int row = 0; row < oldHeight; row++) {
			// old column
			for (int column = 0; column < oldWidth; column++) {
				// get the value in the old picture
				int value = pixels[row][column];

				// rotate picture by 90�
				rotatedPixels[column][newWidth - row - 1] = value;
			}
		}
		return rotatedPixels;
	}

	// mirroring an image horizontally (left to right, right to left)
	static int[][] mirror(int[][] pixels) {
		int totalColumns = pixels[0].length;
		int totalRows = pixels.length;
		int[][] mirroredPixels = new int[totalRows][totalColumns];
		
		// row
		for (int row = 0; row < totalRows; row++) {
			// column
			for (int column = 0; column < totalColumns; column++) {				
				mirroredPixels[row][totalColumns-column-1] = pixels[row][column];
			}
		}

		return mirroredPixels;
	}

	static int[][] gray(int[][] pixels) {
		int totalColumns = pixels[0].length;
		int totalRows = pixels.length;
		
		// color holder
		int red, green, blue, avg, grey;
		
		int[][] greyedPixels = new int[totalRows][totalColumns];
				
		// row
		for (int row = 0; row < totalRows; row++) {
			// column
			for (int column = 0; column < totalColumns; column++) {
				
				// Get current color (RGB)
				int rgb = pixels[row][column];  
				
				// Extract colors
				red = getRed(rgb);
				green = getGreen(rgb);
				blue = getBlue(rgb);
				
				// Average of the colors
				avg = (red + green + blue) / 3;
				
				// get grey color
				grey = getGrey(avg);
				
				// store new grey color
				greyedPixels[row][column] = grey;
			}
		}
		return greyedPixels;
	}
	
	// Get Grey RGB color
	private static int getGrey(int avg) {
		int red32 = avg << 16;
		int green32 = avg << 8;
		int blue = avg;
		
		int rgb = red32 | green32 | blue; // or red32 + green32 + blue
		return rgb;
	}

	static int getRed(int rgb){
		return rgb << 8 >>> 24;
	}
	
	static int getGreen(int rgb){
		return rgb << 16 >>> 24;
	}
	
	static int getBlue(int rgb){	
		return rgb << 24 >>>24;
	}
	
	public static void printToBinary(int num) {
		System.out.println(Integer.toBinaryString(num));
	}
}
