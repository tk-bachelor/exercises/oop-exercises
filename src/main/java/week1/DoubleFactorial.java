package week1;
import java.math.BigInteger;
import java.util.Scanner;

/**
 * @author Keerthikan
 * Week 1 -  Exercise 1
 */
public class DoubleFactorial {

	public static void main(String[] args) {
		
		// Needed variables
		int number;
		int current;
		BigInteger product = BigInteger.ONE;
		
		// Get value from user
		Scanner scanner = new Scanner(System.in);
		number = scanner.nextInt();
		
		// Define start value		
		current = (number % 2 == 0) ? 2 : 1;
		
		long startTime = System.currentTimeMillis();
		
		// calculate double faculty
		for(; current<=number; current +=2)
			product = product.multiply(BigInteger.valueOf(current));
		
		long stopTime = System.currentTimeMillis();
		
		// print out result
		System.out.println(product);
		System.out.println("Calculated Time : " + (stopTime-startTime));
		
		// close scanner
		scanner.close();
	}

}
