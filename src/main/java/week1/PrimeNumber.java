package week1;
import java.util.Scanner;

/**
 * @author Keerthikan
 * Week 1 - Exercise 2
 */
public class PrimeNumber {

	public static void main(String[] args) {
		
		// Result holder		
		boolean isPrime = true;
		
		// Get the number to be checked
		Scanner scanner = new Scanner(System.in);
		int number = scanner.nextInt();
		
		// Loop through to check the prime number
		for(int counter=2; counter<= number / 2;counter++){
			// check if the given number can be divided without any rest
			if(number % counter == 0){
				// Division without rest is successful
				// Therefore it's not a prime number
				isPrime = false;
				break;
			}				
		}
		
		// Result Text
		String result;
		if(isPrime){
			result = String.format("%d ist eine %d Primzahl", number);
		}else{
			result = String.format("%d ist keine Primzahl", number);
		}
		
		// Print out the result
		System.out.println(result);
		
		scanner.close();
	}
	
	
	
}
