package week1;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * 
 * @author Keerthikan
 * 
 *         Week1 Exercice: Theory Questions (Self-Study)
 */
public class TheoryQuestions {

	static int x = 1;
	static int y = 2;

	// Expression A
	@Test
	public void testExpression1() {
		int result = 2 * x - 3 * y - 1;
		int expected = -5;

		assertEquals("Expression 1", expected, result);
	}

	// Expression B
	@Test
	public void testExpression2() {
		int result = 1 - ((x)) + -y;
		int expected = -2;

		assertEquals("Expression 2", expected, result);
	}

	// Expression C
	@Test
	public void testExpression3() {
		int result = 1 + -+x + -1;
		int expected = -1;

		assertEquals("Expression 3", expected, result);
	}

	// Expression Da
	@Test
	public void testExpression4a() {
		int result = x % y * 2;
		int expected = 2;

		assertEquals("Expression 4a", expected, result);
	}

	// Expression D
	@Test
	public void testExpression4() {
		int result = y * x / y + x % y;
		// x + x % y
		int expected = 2;

		assertEquals("Expression 4", expected, result);
	}

	// Expression E
	@Test
	public void testExpression5() {
		boolean result = x == 0;
		boolean expected = false;

		assertEquals("Expression 5", expected, result);
	}

	// Expression F
	@Test
	public void testExpression6() {
		// boolean result = 1 < y < 3; compiler error --> invalid
	}

	// Expression G
	@Test
	public void testExpression7() {
		boolean result = (x > 0) && (x < 2);
		boolean expected = true;

		assertEquals("Expression 7", expected, result);
	}

	// Expression H
	@Test
	public void testExpression8() {
		int result = x++ + x + y-- - --y;
		// 1 + 2 + 2 - 0
		int expected = 5;

		assertEquals("Expression 8", expected, result);
	}

}
