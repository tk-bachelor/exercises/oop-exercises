package week1;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Keerthikan Week 1 - Exercise 3
 * 
 *         Sieb des Eratosthenes
 */
public class PrimeEratosthenes {
	
	static long startTime;
	static long stopTime;

	public static void main(String[] args) {

		// Maximum number
		int number;

		// Get the maximum number
		Scanner scanner = new Scanner(System.in);
		while ((number = scanner.nextInt()) < 2) {
			System.out.println("Bitte geben Sie richtige Zahl!");
		}
		
		startTime = System.currentTimeMillis();
		
		// Container
		ArrayList<Integer> container = new ArrayList<>();
		// fill in all numbers
		for (int counter = 2; counter <= number; counter++) {
			container.add(counter);
		}
		printValues(container);

		// remove all the non-prime numbers with
		// the Sieb des Eratosthenes method
		container = filterOutNonPrimeNumbers(container);
		
		stopTime = System.currentTimeMillis();
		
		// print values after removal
		printValues(container);
		
		// show Elapsed Time
		System.out.println("Execution Time (ms): " + (stopTime - startTime));

		scanner.close();
	}

	/**
	 * Filter out non-prime numbers
	 * 
	 * <pre>
	 * 		1. Man schreibe die ersten N Zahlen in einer Tabelle auf, z.B. f�r N = 20
	 * 		2. Die erste Zahl, 2, ist eine Primzahl. Danach streicht man alle Vielfachen dieser Zahl. 
	 * 		3. Die n�chste nicht-gestrichene Zahl, jetzt 3, ist die n�chste Primzahl. 
	 * 		   Danach streicht man wiederum alle Vielfachen dieser Zahl. 
	 * 		   (Kein Problem, falls die Zahl schon vorher gestrichen wurde.)
	 * 		4. Dies wird so lange wiederholt, bis man die letzte nicht-gestrichene Zahl der Tabelle erreicht hat. 
	 * 		   Alle nicht-gestrichenen Zahlen sind dann Primzahlen.
	 * </pre>
	 * 
	 * @param container
	 *            holds all the numbers
	 * @return 
	 */
	public static ArrayList<Integer> filterOutNonPrimeNumbers(ArrayList<Integer> container) {
		// Go through all the numbers
		for (int counter = 2; counter <= container.get(container.size()-1); counter++) {
			
			for (int pos = 0; pos < container.size(); pos++) {
				// Get the dividend
				int value = container.get(pos);

				// skip the counter value or zero				
				if (value == counter || value == 0)
					continue;
				
				// check the rest
				if (container.get(pos) % counter == 0) {
					// mark this number with 0
					container.set(pos, 0);
				}
			}
		}
		
		// put only the prime numbers
		ArrayList<Integer> primeNumbers = new ArrayList<>();
		for(int value : container){
			if (value != 0) {
				primeNumbers.add(value);
			}
		}
			
		return primeNumbers;
	}

	/**
	 * Print values to double-check
	 * @param container
	 */
	public static void printValues(ArrayList<Integer> container) {
		for (int value : container) {
			System.out.print(", " + value);
		}
		System.out.println("");
	}
}
