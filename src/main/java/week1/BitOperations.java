package week1;

/**
 * 
 * @author Keerthikan
 * 
 *         Learning Bit Operations
 */
public class BitOperations {

	public static void main(String[] args) {

		int x = 414; // 1 1001 1110
		int y = 135; // 1000 0111

		// print binary
		printToBinary("x", x);
		printToBinary("y", y);

		emptyLine();

		// bit complementary
		// 11111111111111111111111001100001
		int xComp = ~x;
		printToBinary("X comp", xComp);

		// Left shift --> 1 1001 1110 0000
		int xLeftShift = x << 4;
		printToBinary("X left shift", xLeftShift);

		// Right shift --> 1 1001
		int xRightShift = x >> 4;
		printToBinary("X right shift", xRightShift);

		// Right shift --> 1 1001
		int xRightShift2 = x >>> 4;
		printToBinary("X right shift 2", xRightShift2);
		
		testColors();
	}

	private static void testColors() {
		// R 238 -> 1110 1110
		// G 139 -> 1000 1011
		// B 059 -> 0011 1011
		
		// RGB Color 0000 0000 1110 1110 1000 1011 0011 1011
		int pixel = 15633211;
		
		int red = (pixel >> 16);
		printToBinary("Red:", red);
		
		int green = (pixel << 16) >>> 24;
		printToBinary("Green:", green);
		
		int blue = (pixel << 24)>>>24;
		printToBinary("Blue:", blue);				
	}

	public static void printToBinary(int num) {
		System.out.println(Integer.toBinaryString(num));
	}

	public static void printToBinary(String name, int num) {
		System.out.println(name + ": " + num + " as " + Integer.toBinaryString(num));
	}

	public static void emptyLine() {
		System.out.println();
	}
}
