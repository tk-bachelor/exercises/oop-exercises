package week7_1_banking;

import week12_reflection.Hidden;

/**
 * 
 * @author Keerthikan
 *
 */
public class BankAccount {
	
	@Hidden
	public static final long DEFAULT_CREDIT_LIMIT = -1000;

	private final long accountId;
	private double balance = 0;
	private long creditLimit;
	private boolean frozen = false;

	public BankAccount(long accountId) {
		this(accountId, DEFAULT_CREDIT_LIMIT);
	}

	public BankAccount(long accountId, long creditLimit) {
		this.accountId = accountId;
		setCreditLimit(creditLimit);
	}
	
	/**
	 * 
	 * @param amount
	 * @throws BankLimitException
	 */
	public void withdraw(double amount) throws BankLimitException {
		if (amount < 0) {
			throw new IllegalArgumentException("Amount must be 0 or positive");
		}
		if (isFrozen()) {
			throw new BankAccessException("Account is frozen");
		}
		if (balance < creditLimit + amount) {
			throw new BankLimitException("Insufficient credit");
		}
		balance -= amount;
	}
	
	public boolean canWithdraw(double amount){
		if (amount < 0) {
			return false;
		}
		if (isFrozen()) {
			return false;
		}
		if (balance < creditLimit + amount) {
			return false;
		}
		return true;
	}

	public void deposit(double amount) throws BankLimitException {
		if (amount < 0) {
			throw new IllegalArgumentException("Amount must be 0 or positive");
		}
		if (isFrozen()) {
			throw new BankAccessException("Account is frozen");
		}
		if (Long.MAX_VALUE - amount < balance) {
			throw new BankLimitException("Balance overflow");
		}
		balance += amount;
	}
	
	@Hidden
	private double round(double value) {
		return (double) (Math.round(value*100.0)/100.0);
	}
	
	public boolean canDeposit(double amount) {
		if (amount < 0) {
			return false;
		}
		if (isFrozen()) {
			return false;
		}
		if (Double.MAX_VALUE - amount < balance) {
			return false;
		}
		return true;
	}

	public long getDeptLimit() {
		return creditLimit;
	}

	public void setCreditLimit(long deptLimit) {
		if (deptLimit > 0) {
			throw new IllegalArgumentException("Credit limit must be zero or negative");
		}
		this.creditLimit = deptLimit;
	}

	public double getBalance() {
		return balance;
	}

	public long getAccountId() {
		return accountId;
	}

	public boolean isFrozen() {
		return frozen;
	}

	public void freeze() {
		frozen = true;
	}

	public void thaw() {
		frozen = false;
	}
	
	public void setBalance(double amount) {
		balance = amount;
	}
}
