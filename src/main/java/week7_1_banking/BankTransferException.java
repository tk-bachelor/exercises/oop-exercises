package week7_1_banking;
public class BankTransferException extends Exception {
	private static final long serialVersionUID = 3412183939840612792L;

	public BankTransferException(String message) {
		super(message);
	}
}
