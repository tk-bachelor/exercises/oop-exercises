package week7_1_banking;

import java.util.HashMap;
import java.util.Map;

public class Bank {
	private final Map<Long, BankAccount> accounts = new HashMap<>();
	private static long idCounter = 0;

	public BankAccount openAccount() {
		BankAccount account = new BankAccount(generateId());
		accounts.put(account.getAccountId(), account);
		return account;
	}

	public void transfer(BankAccount from, BankAccount to, long amount) throws BankTransferException {
		if (!from.canWithdraw(amount))
			throw new BankTransferException(from.getAccountId() + " can't withdraw this amount");

		if (!to.canDeposit(amount))
			throw new BankTransferException("Amount cannot be deposited to " + to.getAccountId());

		try {
			// withdrawal
			from.withdraw(amount);

			// deposit
			try {
				to.deposit(amount);
			} catch (Exception ex) {
				from.deposit(amount);
			}
		} catch (Exception ex) {
			throw new BankTransferException(ex.getMessage());
		}
	}

	public BankAccount getAccount(long id) {
		return accounts.get(id);
	}

	private long generateId() {
		return ++idCounter;
	}

	public void finalizeQuarter(double debitInterest, double creditInterest, double fees) throws BankLimitException {
		for (BankAccount account : accounts.values()) {

			// debit interest
			if (account.getBalance() >= 0) {
				double debitInt = account.getBalance()*debitInterest/100;
				account.deposit(debitInt);
			}else{
				double creditInt = account.getBalance()*creditInterest/100;
				account.withdraw(Math.abs(creditInt));
			}

			if (account.getBalance() < 1000) {
				account.withdraw(fees);
			}
		}
	}
}
