package week11_stream;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamUtil {
	public static <L, R> Stream<Pair<L, R>> zip(Stream<L> first, Stream<R> second) {
		List<L> colFirst = first.collect(Collectors.toList());
		List<R> colSecond = second.collect(Collectors.toList());

		if (colFirst.size() != colSecond.size())
			throw new IllegalArgumentException("Stream lengths don't match");

		List<Pair<L, R>> list = new ArrayList<Pair<L, R>>();
		for (int i = 0; i < colFirst.size(); i++) {
			list.add(new Pair<L, R>(colFirst.get(i), colSecond.get(i)));
		}

		return list.stream();
	}

	public static <L, R> Pair<Stream<L>, Stream<R>> unzip(Stream<Pair<L, R>> combined) {
		List<L> colFirst = new ArrayList<L>();
		List<R> colSecond = new ArrayList<R>();

		combined.forEach(p -> {
			colFirst.add(p.getFirst());
			colSecond.add(p.getSecond());
		});

		Pair<Stream<L>, Stream<R>> pair = new Pair<Stream<L>, Stream<R>>(colFirst.stream(), colSecond.stream());

		return pair;
	}

	public static <L, R> Stream<Pair<L, Stream<R>>> group(Stream<Pair<L, R>> combined) {

		Map<L, List<Pair<L, R>>> grouped = combined
				.collect(Collectors.groupingBy((Pair<L, R> pair) -> pair.getFirst()));
//
//		Map<L, List<Pair<L, R>>> grouped2 = combined
//				.collect(Collectors.groupingBy((Pair<L, R> pair) -> pair.getFirst()));
		// mapping(Person::getLastName, toSet())
		
		List<Pair<L, Stream<R>>> pairs = new ArrayList<>();

		grouped.forEach((l, values) -> {
			List<R> rightVals = new ArrayList<>();
			values.forEach(p -> {
				rightVals.add(p.getSecond());
			});
			pairs.add(new Pair<L, Stream<R>>(l, rightVals.stream()));
		});

		return pairs.stream();
	}
}
