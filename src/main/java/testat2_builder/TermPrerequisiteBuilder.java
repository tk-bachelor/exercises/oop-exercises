package testat2_builder;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import testat2_studienplan.Module;
import testat2_studienplan.Term;

public class TermPrerequisiteBuilder implements TermBuilder{

	@Override
	public Set<Term> buildStudyPlan(Set<Module> modules) {
		Set<Term> terms = new TreeSet<>();

		do {
			Term newTerm = new Term(terms.size() + 1);
			terms.add(newTerm);
			buildTerm(newTerm, modules);
		} while (!modules.isEmpty());

		return terms;
	}

	private void buildTerm(Term newTerm, Set<Module> modules) {
		// find modules without prerequisites
		Set<Module> chosenModules = findModulesWithoutPrerequisities(modules);

		if (chosenModules.isEmpty()) {
			String message = String.format("Term %d - No modules without prerequisites", newTerm.getTermNr());
			throw new IllegalArgumentException(message);
		}

		// remove the chosen modules from other modules
		removePrerequisites(modules, chosenModules);
		
		// add chosen modules to term
		newTerm.addModules(chosenModules);
	}

	private void removePrerequisites(Set<Module> modules, Set<Module> chosenModules) {
		chosenModules.forEach(chosenModule -> {
			modules.forEach(module -> {
				module.removeModule(chosenModule);
			});
		});
	}

	private Set<Module> findModulesWithoutPrerequisities(Set<Module> modules) {
		Set<Module> withoutPrereqs = new HashSet<>();
		Iterator<Module> iterator = modules.iterator();
		while (iterator.hasNext()) {
			Module module = iterator.next();
			if (module.getModules().isEmpty()) {
				withoutPrereqs.add(module);
				iterator.remove();
			}
		}
		return withoutPrereqs;
	}

}
