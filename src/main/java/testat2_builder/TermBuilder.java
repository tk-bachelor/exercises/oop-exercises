package testat2_builder;

import java.util.Set;

import testat2_studienplan.Module;
import testat2_studienplan.Term;

public interface TermBuilder {
	public Set<Term> buildStudyPlan(Set<Module> modules);
}
