package testat2_builder;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import testat2_studienplan.Module;

/**
 * ModuleBuilder is a helper class to build modules with prerequisite /
 * successor modules from the given data.
 * 
 * @author Keerthikan
 *
 */
public class ModuleBuilder {
	private final Map<String, String[]> data;

	public ModuleBuilder(Map<String, String[]> data) {
		this.data = data;
	}

	/**
	 * Build modules with prerequisites
	 * 
	 * @return set of modules with prerequisites
	 */
	public Set<Module> buildPrerequisites() {
		Set<Module> modules = buildModules();

		// loop through modules
		modules.forEach(module -> {
			String[] prerequisites = data.get(module.getName());

			if (prerequisites != null) {
				for (String strPreReq : prerequisites) {
					Module mPreReq = this.findByName(modules, strPreReq);
					module.addModule(mPreReq);
				}
			}
		});
		return modules;
	}

	/**
	 * Build modules with successors
	 * 
	 * @return set of modules with successors
	 */
	public Set<Module> buildSuccessors() {
		Set<Module> modules = buildModules();

		// loop through modules
		data.forEach((strModule, prerequisites) ->{
			if(prerequisites != null){
				Module module = this.findByName(modules, strModule);
				
				for (String strPreReq : prerequisites) {
					Module mPreReq = this.findByName(modules, strPreReq);
					
					// add module as successor
					mPreReq.addModule(module);
				}
			}
		});
		
		return modules;
	}

	/**
	 * Build set of module objects from data
	 * 
	 * @return set of module objects
	 */
	private Set<Module> buildModules() {
		Set<Module> modules = new HashSet<>();

		// loop through lines of data
		data.forEach((key, value) -> {
			int totalPrerequisites = value == null ? 0 : value.length;
			Module m = new Module(key, totalPrerequisites);
			modules.add(m);
		});
		return modules;
	}

	/**
	 * find module by its name
	 * 
	 * @param modules
	 *            set of modules to search in
	 * @param name
	 *            search keyword
	 * @return module with the same name
	 */
	public Module findByName(Set<Module> modules, String name) {
		Iterator<Module> it = modules.iterator();
		while (it.hasNext()) {
			Module m = it.next();
			if (m.getName().equals(name)) {
				return m;
			}
		}
		return null;
	}

}
