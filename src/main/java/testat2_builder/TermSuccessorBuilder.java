package testat2_builder;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import testat2_studienplan.Module;
import testat2_studienplan.Term;

public class TermSuccessorBuilder implements TermBuilder {

	@Override
	public Set<Term> buildStudyPlan(Set<Module> modules) {
		Set<Term> terms = new TreeSet<>();

		Set<Module> possibleSuccessors = null;
		do {
			Term newTerm = new Term(terms.size() + 1);
			terms.add(newTerm);

			possibleSuccessors = buildTerm(newTerm, modules, possibleSuccessors);
		} while (!possibleSuccessors.isEmpty());

		return terms;
	}

	private Set<Module> buildTerm(Term newTerm, Set<Module> modules, Set<Module> possibleSuccessors) {
		// find modules without prerequisites
		if (possibleSuccessors == null) {
			possibleSuccessors = findModulesWithoutPrerequisities(modules);
		} else {
			possibleSuccessors = findModulesWithoutPrerequisities(possibleSuccessors);
		}

		if (possibleSuccessors.isEmpty()) {
			String message = String.format("Term %d - No modules without prerequisites", newTerm.getTermNr());
			throw new IllegalArgumentException(message);
		}

		// add chosen modules to term
		newTerm.addModules(possibleSuccessors);

		// get new possible successors
		possibleSuccessors = getNextPossibleSuccessors(possibleSuccessors);

		return possibleSuccessors;
	}

	private Set<Module> getNextPossibleSuccessors(Set<Module> chosenModules) {
		Set<Module> nextPossibleSuccessors = new HashSet<>();
		
		// get all successors from all chosen modules
		chosenModules.forEach(chosenModule ->{
			nextPossibleSuccessors.addAll(chosenModule.getModules());			
		});
		
		// decrease total prerequisites
		Iterator<Module> it = nextPossibleSuccessors.iterator();
		while(it.hasNext()){
			Module possibleSuccessor = it.next();
			possibleSuccessor.decreasePrerequisites(chosenModules);
		}
		
		return nextPossibleSuccessors;
	}

	private Set<Module> findModulesWithoutPrerequisities(Set<Module> successors) {
		Set<Module> withoutPrereqs = new HashSet<>();
		Iterator<Module> iterator = successors.iterator();
		while (iterator.hasNext()) {
			Module module = iterator.next();
			if (module.getTotalPrerequisites() == 0) {
				withoutPrereqs.add(module);
				iterator.remove();
			}
		}
		return withoutPrereqs;
	}

}
