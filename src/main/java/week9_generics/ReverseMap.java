package week9_generics;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ReverseMap<L, R>{
	
	private Map<L, R> leftMap;
	private Map<R, L> rightMap;
	
	public ReverseMap(){
		leftMap = new HashMap<>();
		rightMap = new HashMap<>();
	}
	
	public void put(L left, R right){
		leftMap.put(left, right);
		rightMap.put(right, left);
	}
	
	public R getRight(L left){
		return leftMap.get(left);
	}
	
	public L getLeft(R right){
		return rightMap.get(right);
	}
	
	public Iterator<L> leftValues(){
		return rightMap.values().iterator();
	}
	
	public Iterator<R> rightValues(){
		return leftMap.values().iterator();
	}
	
	public int size(){
		return leftMap.size();
	}
	
	public void clear(){
		leftMap.clear();
		rightMap.clear();
	}	
}
