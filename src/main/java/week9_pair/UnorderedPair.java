package week9_pair;

public class UnorderedPair<T extends Comparable<T>> implements Pair<T> {
	private final T first;
	private final T second;

	public UnorderedPair(T first, T second) {
		if (first == null || second == null) {
			throw new IllegalArgumentException("null argument");
		}
		this.first = first;
		this.second = second;
	}

	@Override
	public T getFirst() {
		return first;
	}

	@Override
	public T getSecond() {
		return second;
	}

	@Override
	public int hashCode() {
		int result = 0;
		result += (first == null) ? 0 : first.hashCode();
		result += (second == null) ? 0 : second.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		UnorderedPair other = (UnorderedPair) obj;
		return equals(first, other.first) && equals(second, other.second)
				|| equals(first, other.second) && equals(second, other.first);
	}

	private boolean equals(Object obj1, Object obj2) {
		if (obj1 == null || obj2 == null) {
			return obj1 == obj2;
		} else {
			return obj1.equals(obj2);
		}
	}

	@Override
	public UnorderedPair<T> clone() {
		return new UnorderedPair<T>(first, second);
	}

	@Override
	public int compareTo(Pair<T> o) {
		// UnorderedPair
		if (o instanceof UnorderedPair) {
			int firstFirst = getFirst().compareTo(o.getFirst());
			int firstSecond = getFirst().compareTo(o.getSecond());
			int secondSecond = getSecond().compareTo(o.getSecond());
			int secondFirst = getSecond().compareTo(o.getFirst());
			
			if(firstFirst == firstSecond &&
					firstSecond == secondSecond
						&& secondSecond == secondFirst){
				return firstFirst;
			}else{
				return 0;
			}
		}
		// OrderedPair
		return 1;
	}

}
