package week9_pair;

public interface Pair<T extends Comparable<T>> extends Cloneable, Comparable<Pair<T>> {
	T getFirst();
	T getSecond();
	Pair<T> clone();
}
