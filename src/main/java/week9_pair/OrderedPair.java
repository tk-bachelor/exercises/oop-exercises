package week9_pair;

public class OrderedPair<T extends Comparable<T>> implements Pair<T> {
	private final T first;
	private final T second;

	public OrderedPair(T first, T second) {
		if (first == null || second == null) {
			throw new IllegalArgumentException("null argument");
		}
		this.first = first;
		this.second = second;
	}

	@Override
	public T getFirst() {
		return first;
	}

	@Override
	public T getSecond() {
		return second;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((first == null) ? 0 : first.hashCode());
		result = prime * result + ((second == null) ? 0 : second.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}

		OrderedPair<T> other = (OrderedPair<T>) obj;
		if (first == null) {
			if (other.first != null) {
				return false;
			}
		} else if (!first.equals(other.first)) {
			return false;
		}
		if (second == null) {
			if (other.second != null) {
				return false;
			}
		} else if (!second.equals(other.second)) {
			return false;
		}
		return true;
	}

	@Override
	public OrderedPair<T> clone() {
		return new OrderedPair<T>(first, second);
	}

	@Override
	public int compareTo(Pair<T> o) {
		// OrderedPair
		if (o instanceof OrderedPair) {
			int firstResult = getFirst().compareTo(o.getFirst());
			if (firstResult == 0) {
				return getSecond().compareTo(o.getSecond());
			}
			return firstResult;
		}
		// UnorderedPair{
		return -1;
		
	}
}
