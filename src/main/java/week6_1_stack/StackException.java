package week6_1_stack;

public class StackException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public StackException() {
	}
	
	public StackException(String message){
		super(message);
	}

}
