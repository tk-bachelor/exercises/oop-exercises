package week6_1_stack;

public class StackFullException extends StackException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public StackFullException(){
		super("Stack is full");
	}
	
	public StackFullException(String message){
		super(message);
	}	
}
