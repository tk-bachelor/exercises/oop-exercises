package week6_1_stack;

public class Stack {
	private final static int MIN_CAPACITY = 0;
	private final static int MAX_CAPACITY = 65536;
	
	private int capacity;
	private int pointer;
	
	private Object[] objects;
	
	public Stack(int capacity) throws StackCapacityException{
		
		// Capacity check
		if(capacity <= MIN_CAPACITY || capacity >=MAX_CAPACITY)
			throw new StackCapacityException("Capacity should be between 0 and 65536");
		
		this.capacity = capacity;		
		objects = new Object[capacity];
		pointer = -1;
	}
	
	public void push(Object element) throws ObjectNullException, StackFullException{
		if(element == null){
			throw new ObjectNullException();
		}
		
		if(pointer + 1 == capacity){
			throw new StackFullException();
		}
		
		objects[++pointer] = element;
	}
	
	public Object pop() throws StackEmptyException{
		if(pointer == -1)
			throw new StackEmptyException();
		
		Object obj = objects[pointer];
		objects[pointer--] = null;
		return obj;
	}
	
	public int size(){
		return pointer + 1;
	}
	
	public int capacity(){
		return capacity;
	}
	
}
