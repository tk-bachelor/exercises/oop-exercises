package week6_1_stack;

public class StackCapacityException extends StackException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public StackCapacityException(){
		
	}
	
	public StackCapacityException(String message){
		super(message);
	}

}
