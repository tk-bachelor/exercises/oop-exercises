package week6_1_stack;

public class StackEmptyException extends StackException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public StackEmptyException(){
		
	}
	
	public StackEmptyException(String exception){
		super(exception);
	}
	
	
}
