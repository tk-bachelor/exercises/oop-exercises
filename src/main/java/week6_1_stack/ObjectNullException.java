package week6_1_stack;

public class ObjectNullException extends StackException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ObjectNullException(){
		super("Object is null");
	}
	
	public ObjectNullException(String message){
		super(message);
	}	
}
