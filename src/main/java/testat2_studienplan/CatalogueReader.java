package testat2_studienplan;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

public class CatalogueReader implements AutoCloseable {
	private BufferedReader reader;

	public CatalogueReader(String filePath) throws FileNotFoundException, UnsupportedEncodingException {
		// Umlaut compatibile
		String path = getClass().getClassLoader().getResource(filePath).getPath();
		path = URLDecoder.decode(path, "UTF-8");
		reader = new BufferedReader(new FileReader(path));
	}

	// null if end of input reached
	public String[] readNexteLine() throws IOException {
		String line;
		line = reader.readLine();
		if (line != null) {
			return line.split(" ");
		} else {
			return null;
		}
	}

	/**
	 * Additional Method created by Keerthikan.T
	 * @return
	 * @throws IOException
	 */
	public Map<String, String[]> readAll() throws IOException {
		Map<String, String[]> lines = new HashMap<>();
		String line;
		while ((line = reader.readLine()) != null) {
			String[] splitedLine = line.split(" ", 2);
			String [] value = (splitedLine.length > 1) ? splitedLine[1].split(" ") : null;  
			lines.put(splitedLine[0], value);
		}
		return lines;
	}

	@Override
	public void close() throws Exception {
		reader.close();
	}
}
