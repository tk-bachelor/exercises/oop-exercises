package testat2_studienplan;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Module has a unique name and prerequisite/ successor modules.
 * 
 * @author Keerthikan
 *
 */
public class Module {
	private final String name;
	private final Set<Module> modules;
	private int totalPrerequisites;

	public Module(String name, int totalPrerequisites) {
		this.name = name;
		this.totalPrerequisites = totalPrerequisites;
		this.modules = new HashSet<Module>();
	}

	public String getName() {
		return name;
	}
	
	public void decreasePrerequisites(Set<Module> chosenModules){
		chosenModules.forEach(chosenModule ->{
			if(chosenModule.getModules().contains(this)){
				totalPrerequisites--;
			}
		});
	}
	
	public int getTotalPrerequisites(){
		return totalPrerequisites;
	}

	/**
	 * Add a prerequisite or successor module into the container
	 * 
	 * @param module
	 *            prerequisite or successor module
	 */
	public void addModule(Module module) {
		this.modules.add(module);
	}

	/**
	 * Add a prerequisite or successor module into the container
	 * 
	 * @param module
	 *            prerequisite or successor module
	 */
	public void removeModule(Module m) {
		this.modules.remove(m);
	}

	/**
	 * Returns a unmodifiable set of prerequisite or successor modules
	 * 
	 * @return prerequisite or successor modules
	 */
	public Set<Module> getModules() {
		return Collections.unmodifiableSet(modules);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());

		// !!! adding modules set into hashcode will cause a problem when it is
		// modified
		
		// result = prime * result + ((prerequisites == null) ? 0 :
		// prerequisites.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Module other = (Module) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;

		return true;
	}

	@Override
	public String toString() {
		return name;
	}

}
