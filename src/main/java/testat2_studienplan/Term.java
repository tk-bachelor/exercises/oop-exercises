package testat2_studienplan;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Term implements Comparable<Term> {
	private final int termNr;
	private final Set<Module> modules;

	public Term(int termNr) {
		this.termNr = termNr;
		this.modules = new HashSet<>();
	}

	/**
	 * @return the termNr
	 */
	public int getTermNr() {
		return termNr;
	}

	public void addModule(Module module) {
		this.modules.add(module);
	}

	public void addModules(Set<Module> modules) {
		this.modules.addAll(modules);
	}

	public Set<Module> getModules() {
		return Collections.unmodifiableSet(modules);
	}

	public void print() {
		String term = "Semester " + termNr + ":";
		for (Module module : modules) {
			term += " " + module.getName();
		}
		System.out.println(term);
	}

	@Override
	public int compareTo(Term o) {
		return this.termNr - o.termNr;
	}

}
