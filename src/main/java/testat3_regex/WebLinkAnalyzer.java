package testat3_regex;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WebLinkAnalyzer {

	// private final String linkRegex = "(href|src)=\"(?<url>[^\\s].+?)\"";
	// private final String linkRegex =
	// "(<a|<img).*?(href|src)=\"(?<url>[^\\s].+?)\"";
	private final String linkRegex = "(<a|<img).*?(href|src)=\"(?<url>[^\\s\"#].+?)\".*?(>|/>)";

	private Pattern pattern = Pattern.compile(linkRegex);

	public List<String> analyseLinks(String url) throws IOException {
		URL uri = new URL(url);

		StringBuilder buffer = new StringBuilder();
		try (InputStream stream = uri.openStream()) {
			int value = stream.read();
			while (value >= 0) {
				char c = (char) value;
				buffer.append(c);
				value = stream.read();
			}
		}
		return extractLinks(buffer.toString());
	}

	private List<String> extractLinks(String content) {
		List<String> links = new ArrayList<>();
		Matcher matcher = pattern.matcher(content);

		while (matcher.find()) {
			String match = matcher.group("url");
			links.add(match);
		}
		return links;
	}

	public void printNotReachableLinks(String domain, List<String> urls) {

		for (String foundUrl : urls) {
			String urlToCheck = foundUrl.contains(domain) ? foundUrl : domain + foundUrl;
			try {
				URL url = new URL(urlToCheck);
				try (InputStream stream = url.openStream()) {
					System.out.println("OK - " + urlToCheck);
				} catch (IOException e) {
					System.err.println("Not Found - " + foundUrl);
				}
			} catch (MalformedURLException e1) {
				System.err.println("Invalid URL - " + urlToCheck);
			}
		}
	}
}
