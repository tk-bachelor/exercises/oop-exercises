package week5;

import java.awt.Point;

/**
 * 
 * @author Keerthikan
 * Week 5 - Interfaces
 * Exercise 1
 */
public interface GraphicItem {
	int getX();
	int getY();
	void move(Point movedPoint);
	
	default void moveToCenter(){
		move(new Point(0, 0));
	}
	
	default boolean isInCenter(){
		return getX() == 0 && getY() == 0;
	}
	
	// TextBox will encounter a conflict
	// because it inherits the default method from both rectangle class and text interface
	// i.e. the concrete TextBox class should override this default method
	default int getColor(){
		return 0x000000;
	}
}
