package week5;

import java.awt.Point;

import com.sun.glass.ui.Size;

/**
 * 
 * @author Keerthikan
 * Week 5 - Interfaces
 * Exercise 1
 */
public class Rectangle implements Shape{
	private Point topLeft;
	private Size size;
	private int color;
	
	public Rectangle(Point topLeft, Size size, int color) {
		this.topLeft = topLeft;
		this.size = size;
		this.color = color;
	}

	@Override
	public int getX() {
		return topLeft.x;
	}

	@Override
	public int getY() {
		return topLeft.y;
	}

	@Override
	public void move(Point movedPoint) {
		this.topLeft = movedPoint;
	}

	@Override
	public int getAreaColor() {
		return color;
	}
	
	public int getWidth(){
		return size.width;
	}
	
	public int getHeight(){
		return size.height;
	}
	
	public void resize(Size resized){
		this.size = resized;
	}

	@Override
	public void draw(Visualization visualization) {
		visualization.drawRectangle(topLeft.x, topLeft.y, size.width, size.height, color);		
	}
}
