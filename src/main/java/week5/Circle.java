package week5;

import java.awt.Point;

/**
 * 
 * @author Keerthikan
 * Week 5 - Interfaces
 * Exercise 1
 */
public class Circle implements Shape{
	private Point center;
	private int radius, color;
	
	public Circle(Point center, int radius, int color){
		this.center = center;
		this.radius = radius;
		this.color = color;
	}
	
	public int getRadius(){
		return radius;
	}

	@Override
	public int getX() {
		return center.x;
	}

	@Override
	public int getY() {
		return center.y;
	}

	@Override
	public void move(Point movedPoint) {
		this.center = movedPoint;
	}

	@Override
	public int getAreaColor() {
		return color;
	}

	@Override
	public void draw(Visualization visualization) {
		visualization.drawCircle(center.x, center.y, radius, color);		
	}

}
