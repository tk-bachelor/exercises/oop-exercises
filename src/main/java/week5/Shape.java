package week5;

/**
 * 
 * @author Keerthikan
 * Week 5 - Interfaces
 * Exercise 1
 */
public interface Shape extends GraphicItem{
	int getAreaColor();
	void draw(Visualization visualization);
	
	@Override
	default int getColor() {
		return getAreaColor();
	}
}
