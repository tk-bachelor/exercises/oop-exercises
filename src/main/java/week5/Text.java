package week5;

/**
 * 
 * @author Keerthikan
 * Week 5 - Interfaces
 * Exercise 1
 */
public interface Text extends GraphicItem{
	String getText();
	int getTextColor();
	
	@Override
	default int getColor() {
		return getTextColor();
	}
}
