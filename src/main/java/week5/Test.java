package week5;

import java.awt.Point;

import com.sun.glass.ui.Size;

public class Test {
	public static void main(String[] args) {

		// Rectangle
		Point rectangleTopLeft = new Point(10, 10);
		Size rectangeSize = new Size(100, 80);
		Shape rectangle = new Rectangle(rectangleTopLeft, rectangeSize, NiceColors.BLUE.getValue());
		
		// Circle
		Point center = new Point(130, 60);
		Shape circle = new Circle(center, 40, NiceColors.RED.getValue());
		
		// Text
		Point textTopLeft = new Point(30, 110);
		Size textRectSize = new Size(100, 50);
		Shape text = new TextBox(textTopLeft, textRectSize, NiceColors.GREEN.getValue(), "Hello!", NiceColors.ORANGE.getValue());
		
		// Shapes
		Shape[] shapes = { rectangle, circle, text};
		Drawing drawing = new Drawing(shapes);
		
		drawing.move(100, 100);
		drawing.draw();
		drawing.printTexts();
		
		// Multiple inheritance
		Test t = new Test();
		C c = t.new C();
	}
	
	/**
	 * 
	 * @author Keerthikan
	 * Exercise 7 - Multiple Inheritance
	 * Shorthand version
	 */
	public interface A {
		default void f(){
			System.out.println("A::f()");
		}
		
		default void g(){
			System.out.println("A::g()");
		}
	}
	
	public interface B {
		default void h(){
			System.out.println("B::h()");
		}
	}
	
	// inner class
	public class C implements A, B {
		public C(){
			f();
			g();
			h();
		}
	}
}
