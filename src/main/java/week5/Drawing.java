package week5;

import java.awt.Point;

public class Drawing {
	private Shape[] figures;
	
	public Drawing(Shape[] figures){
		this.figures = figures;
	}
	
	public void draw(){
		Visualization v = new Visualization();
		v.clear();
		for(Shape s : figures)
			s.draw(v);
	}
	
	public void move(int deltaX, int deltaY){
		for(Shape s : figures){
			s.move(new Point(s.getX()+deltaX, s.getY()+ deltaY));
		}
	}
	
	public void printTexts(){
		for(Shape s : figures){
			if(s instanceof Text){
				System.out.println(((Text) s).getText());
			}
		}
	}
	
}
