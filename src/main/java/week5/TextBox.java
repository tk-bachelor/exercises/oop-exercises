package week5;

import java.awt.Point;

import com.sun.glass.ui.Size;

/**
 * 
 * @author Keerthikan
 * Week 5 - Interfaces
 * Exercise 1
 */
public class TextBox extends Rectangle implements Text{
	private String text;
	private int textColor;
	
	public TextBox(Point topLeft, Size size, int areaColor, String text, int textColor) {
		super(topLeft, size, areaColor);
		this.text = text;
		this.textColor = textColor;
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	public int getTextColor() {
		return textColor;
	}
	
	@Override
	public void draw(Visualization visualization) {
		super.draw(visualization);
		visualization.drawText(getX(), getY(), text, textColor);
	}

	@Override
	public int getColor() {
		return textColor;
	}

}
