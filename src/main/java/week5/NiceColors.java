package week5;

public enum NiceColors {
	BLUE(0x1f497d),
	RED(0xc0504d),
	GREEN(0x9bbb59),
	ORANGE(0xf79646),
	WHITE(0xffffff),
	BLACK(0x000000);
	
	private int value;
	
	private NiceColors(int value) {
		this.value = value;
	}
	
	public int getValue(){
		return this.value;
	}
}
