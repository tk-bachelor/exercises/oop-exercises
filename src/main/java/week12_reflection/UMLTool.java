package week12_reflection;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Executable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;

import testat1_model.BundleItem;
import week7_1_banking.BankAccount;

public class UMLTool {
	public static void printClasses(String filePath) throws IOException {
		File folder = new File(filePath);
		if (!folder.exists()) {
			throw new IllegalArgumentException("Invalid path: " + folder.getAbsolutePath());
		}
		if (!folder.isDirectory()) {
			throw new IllegalArgumentException("Not a directory: " + folder.getAbsolutePath());
		}
		ClassCollector collector = new ClassCollector(folder);

		for (Class<?> type : collector.allClasses()) {
			printClass(type);
		}
		printClass(BundleItem.class);
		printClass(BankAccount.class);
	}

	private static void printClass(Class<?> type) {
		// Check if hidden
		if (!isHidden(type.getAnnotations())) {

			System.out.println("-------------------------");
			// Class Name
			System.out.println("Class: " + type.getName());

			// Declared fields
			System.out.println("-------------------------");
			for (Field f : type.getDeclaredFields()) {
				if (!isHidden(f.getAnnotations())) {
					System.out.println(String.format("%s %s : %s", getModifierType(f.getModifiers()), f.getName(),
							f.getType().getSimpleName()));
				}
			}
			System.out.println("-------------------------");

			// Constructors
			for (Constructor<?> c : type.getConstructors()) {
				System.out.println(String.format("%s %s(%s)", getModifierType(c.getModifiers()), type.getSimpleName(),
						getParameters(c)));
			}

			// Declared methods
			for (Method m : type.getDeclaredMethods()) {
				if (!isHidden(m.getAnnotations())) {
					System.out.println(String.format("%s %s(%s): %s", getModifierType(m.getModifiers()), m.getName(),
							getParameters(m), m.getReturnType().getSimpleName()));
				}
			}
			System.out.println("-------------------------");
			System.out.println("\n\n");

		} else {
			System.err.println(type.getSimpleName() + " is hidden\n");
		}
	}

	private static String getParameters(Executable e) {
		if (e.getParameterCount() == 0)
			return "";

		String params = "";
		for (Parameter p : e.getParameters()) {
			if (!params.equals("")) {
				params += ", ";
			}
			params += p.getType().getSimpleName();
		}
		return params;
	}

	private static String getModifierType(int modifier) {
		if ((modifier & Modifier.PUBLIC) != 0) {
			return "+";
		} else if ((modifier & Modifier.PROTECTED) != 0) {
			return "#";
		} else if (Modifier.isPrivate(modifier)) {
			return "-";
		} else {
			return "not detected";
		}

		// switch (modifier) {
		// case Modifier.PUBLIC:
		// return "+";
		// case Modifier.PROTECTED:
		// return "#";
		// case Modifier.PRIVATE:
		// return "-";
		// default:
		// return "not detected";
		// }
	}

	private static boolean isHidden(Annotation[] annotations) {
		for (Annotation an : annotations) {
			if (an instanceof Hidden) {
				return true;
			}
		}
		return false;
	}
}
