package week12_reflection;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collection;
import java.util.HashSet;

public class ClassCollector {
	private static final String CLASS_FILE_SUFFIX = ".class";
	
	private File folder; 
	private Collection<Class<?>> classes = new HashSet<>();
	
	public ClassCollector(File folder) throws IOException {
		this.folder = folder;
		collectAllClasses();
	}
	
	public Collection<Class<?>> allClasses() {
		return classes;
	}
	
	private void collectAllClasses() throws IOException {
		URL url = folder.toURI().toURL();
		try (URLClassLoader loader = new URLClassLoader(new URL[] { url })) {
			loadAllClasses(loader, folder, "");
		}
	}
	
	private void loadAllClasses(ClassLoader loader, File folder, String packageIdentifier) {
		for (File file: folder.listFiles()) {
			String fileName = file.getName();
			if (file.isDirectory()) {
				loadAllClasses(loader, file, packageIdentifier + fileName + ".");
			} else if (fileName.endsWith(CLASS_FILE_SUFFIX)) {
				String className = packageIdentifier + fileName.substring(0, fileName.length() - CLASS_FILE_SUFFIX.length());
				try {
					Class<?> type = loader.loadClass(className);
					classes.add(type);
				} catch (ClassNotFoundException e) {
					throw new RuntimeException("Class load failed", e);
				}
			}
		}
	}
}
