package week6_3_exceptions;

public class ExceptionTest {

	public static void main(String[] args) throws Exception {

		System.out.println("a) test1\n");
		// a) x = 0
		try {
			test1(0);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

		// a) x = 1
		try {
			test1(1);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

		// a) x = 2
		try {
			test1(2);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

		// b) x = 0
		System.out.println("b) test2\n");
		try {
			test2(0);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

		// b) x = 1
		try {
			test2(1);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

		// c
		// test3(); // expected = FirstCatch
		
		// d
		//test4(); // expected = NullPointerException
		
		

	}

	static void test1(int x) throws Exception {
		try {
			if (x == 0) {
				throw new Exception();
			} else if (x == 1) {
				throw new MyException();
			} else if (x == 2) {
				throw new MySubException();
			}
		} catch (MySubException e) {
			System.out.println("First catch");
		} catch (MyException e) {
			System.out.println("Second catch");
		} finally {
			System.out.println("Finally");
		}
	}

	static void test2(int x) {
		try {
			try {
				if (x == 0) {
					throw new MyException();
				} else {
					throw new MySubException();
				}
			} catch (MySubException s) {
				System.out.println("Inner");
			}
		} catch (MyException s) {
			System.out.println("Outer");
		}
	}

	static void test3() throws Exception {
		try {
			throw new MyException();
		} catch (MyException e) {
			System.out.println("First catch");
			throw e;
		} catch (Exception e) {
			System.out.println("Second catch");
			throw e;
		}
	}

	static void test4() {
		try {
			throw new RuntimeException("Test");
		} finally {
			((String) null).length();
		}
	}
	
	static void subCall1() throws RuntimeException {}
	static void subCall2() throws MyException {}
	
	static void test5(){
		subCall1();
	}
	
	static void test6() throws MyException{
		subCall2();
	}
}
