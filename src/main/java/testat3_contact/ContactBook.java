package testat3_contact;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import testat3_io.ObjectStreamHelper;

public class ContactBook {
	private final String filename = "contact.bin";
	private Map<String, Contact> contactBook = new HashMap<>();

	public void load() throws ContactBookException {
		try {
			contactBook = ObjectStreamHelper.deserialize(filename);
		} catch (ClassNotFoundException | IOException e) {
			throw new ContactBookException(e.getMessage());
		}
	}

	public void save() throws ContactBookException {
		try {
			ObjectStreamHelper.serialize("contact.bin", contactBook);
		} catch (IOException e) {
			throw new ContactBookException("Failed to save contact book. " + e.getMessage());
		}
	}

	public void addContact(String name, String address) {
		if (contactBook.containsKey(name)) {
			throw new IllegalArgumentException("Name already exists");
		}
		contactBook.put(name, new Contact(name, address));
	}

	public void addNumber(String name, String number, String description) {
		if (!contactBook.containsKey(name)) {
			throw new IllegalArgumentException("Name does not exist");
		}
		Contact contact = contactBook.get(name);
		contact.addPhoneEntry(new PhoneEntry(number, description));
	}

	public Contact findContact(String name) {
		return contactBook.get(name);
	}
}