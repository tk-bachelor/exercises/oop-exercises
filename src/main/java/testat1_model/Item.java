package testat1_model;

/**
 * @author Keerthikan
 * 
 *         Abstract Superclass for different types of items
 */
public abstract class Item {

	// ---------------------------------------------------------
	// Attributes
	// ---------------------------------------------------------

	private String description;

	// ---------------------------------------------------------
	// Constructors
	// ---------------------------------------------------------

	public Item(String description) {
		setDescription(description);
	}

	// ---------------------------------------------------------
	// Getters / Setters
	// ---------------------------------------------------------

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		if (description == null || "".equals(description))
			throw new IllegalArgumentException("Description can not be empty");
		this.description = description;
	}

	// ---------------------------------------------------------
	// Methods
	// ---------------------------------------------------------

	/**
	 * Print out the item details in console
	 */
	public void print() {
		System.out.println("--------------------");
		System.out.println("Item: " + description);
	}

	// ---------------------------------------------------------
	// Abstract methods
	// ---------------------------------------------------------

	/**
	 * Calculates price of the item
	 * 
	 * @return price
	 */
	public abstract double getPrice();

}
