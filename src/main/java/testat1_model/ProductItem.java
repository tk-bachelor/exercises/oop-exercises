package testat1_model;

/**
 * @author Keerthikan
 * 
 */
public class ProductItem extends Item {

	// ---------------------------------------------------------
	// Attributes
	// ---------------------------------------------------------
	private int amount;
	private double pricePerUnit;

	// ---------------------------------------------------------
	// Constructors
	// ---------------------------------------------------------

	public ProductItem(String description, double pricePerUnit) {
		super(description);
		setPricePerUnit(pricePerUnit);
	}

	public ProductItem(String description, double pricePerUnit, int amount) {
		super(description);
		setPricePerUnit(pricePerUnit);
		setAmount(amount);
	}

	// ---------------------------------------------------------
	// Getters / Setters
	// ---------------------------------------------------------

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		if (amount <= 0)
			throw new IllegalArgumentException("Amount must be positive");
		this.amount = amount;
	}

	public double getPricePerUnit() {
		return pricePerUnit;
	}

	public void setPricePerUnit(double pricePerUnit) {
		if (pricePerUnit <= 0)
			throw new IllegalArgumentException("Price per unit must be positive");
		this.pricePerUnit = pricePerUnit;
	}

	// ---------------------------------------------------------
	// Overrided methods
	// ---------------------------------------------------------

	@Override
	public double getPrice() {
		return amount * pricePerUnit;
	}

	/**
	 * Print out the ProductItem details in console
	 */
	@Override
	public void print() {
		super.print();
		System.out.println(String.format("Amount: %d � Fr. %.2f", amount, pricePerUnit));
		System.out.println(String.format("Total Price: Fr. %.2f", getPrice()));
	}

}
