package testat1_model;

import java.util.ArrayList;
import java.util.List;

import week12_reflection.Hidden;

/**
 * @author Keerthikan
 * 
 *         BundleItem is a subclass of abstract Item class. It can hold multiple
 *         items in it.
 */
@Hidden
public class BundleItem extends Item implements Cloneable{

	// ---------------------------------------------------------
	// Attributes
	// ---------------------------------------------------------

	private double discount = 0;
	private List<Item> items;
	private BundleItem parent;

	// ---------------------------------------------------------
	// Constructors
	// ---------------------------------------------------------

	public BundleItem(String description) {
		super(description);
	}

	/**
	 * @param description
	 * @param discount
	 *            in percentage (eg. 70 => 70%)
	 */
	public BundleItem(String description, double discount) {
		super(description);
		setDiscount(discount);
	}

	// ---------------------------------------------------------
	// Getters / Setters
	// ---------------------------------------------------------

	public List<Item> getItems() {
		if (items == null)
			items = new ArrayList<Item>();
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	/**
	 * @return discount in percentage (70 => 70%)
	 */
	public double getDiscount() {
		return discount;
	}

	/**
	 * @param discount
	 *            in percentage (eg. 70 => 70%)
	 */
	public void setDiscount(double discount) {
		if (discount < 0)
			throw new IllegalArgumentException("Discount cannot be negative");
		this.discount = discount;
	}

	public BundleItem getParent() {
		return parent;
	}

	public void setParent(BundleItem parent) {
		this.parent = parent;
	}

	// ---------------------------------------------------------
	// Overrided methods
	// ---------------------------------------------------------

	@Override
	public double getPrice() {
		double totalPrice = 0;

		// Get the total price of items
		for (Item item : getItems()) {
			totalPrice += item.getPrice();
		}

		// subtract the discount
		totalPrice -= totalPrice * discount / 100;
		return totalPrice;
	}

	@Override
	public void print() {
		super.print();
		System.out.println(String.format("Bundle discount: %.2f %%", discount));
		System.out.println(String.format("Bundle Items: %d", getItems().size()));

		System.out.println();
		System.out.println("====================");
		// Items in bundle
		for (Item item : getItems()) {
			item.print();
		}
		System.out.println("====================");
		System.out.println();
	}

	// ---------------------------------------------------------
	// Extended Functions
	// ---------------------------------------------------------
	/**
	 * Adds item to the bundle list. In case a bundle is added, it also
	 * validates ancestor tree in order to prevent bundle cycling
	 * 
	 * @param item
	 *            to be added
	 * @throws IllegalArgumentException
	 *             if the item is directly/indirectly causes bundle cycling
	 *             error
	 */
	public void addItem(Item item) throws IllegalArgumentException {
		// Only BundleItem needed to be checked
		if (item instanceof BundleItem) {
			// check if they are the same bundle
			if (item == this) {
				throw new IllegalArgumentException("Bundle Cycling Error: Direct References " + item.getDescription());
			}
			// check if its ancestor is the same as the given item
			else if (isAncestor((BundleItem) item)) {
				throw new IllegalArgumentException(
						"Bundle Cycling Error: Indirect References " + item.getDescription());
			}

			// set parent
			((BundleItem) item).setParent(this);
		}
		// in case everything's fine, add it
		getItems().add(item);
	}

	/**
	 * Check along the ancestor tree
	 * 
	 * @param item
	 *            bundle to be checked
	 * @return true - an ancestor found with the same reference false - no
	 *         ancestor found
	 */
	public boolean isAncestor(BundleItem item) {
		if (parent == null) {
			return false;
		}
		// check if its parent is the same
		else if (parent.equals(item)) {
			return true;
		}
		// check the ancestor tree
		else {
			return parent.isAncestor(item);
		}
	}
	
	@Override
	public BundleItem clone() throws CloneNotSupportedException {
		return (BundleItem) super.clone();
	}
}
