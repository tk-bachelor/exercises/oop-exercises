package testat1_model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Keerthikan
 * 
 *         Order can hold several different types items
 */
public class Order {

	// ---------------------------------------------------------
	// Attributes
	// ---------------------------------------------------------
	private List<Item> items;

	// ---------------------------------------------------------
	// Getters / Setters
	// ---------------------------------------------------------

	public List<Item> getItems() {
		if (items == null)
			items = new ArrayList<Item>();
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	// ---------------------------------------------------------
	// Custom methods
	// ---------------------------------------------------------

	/**
	 * Add item to the item list
	 * 
	 * @param item
	 *            - Item to be added
	 */
	public void addItem(Item item) {
		getItems().add(item);
	}

	/**
	 * Calculates the total price of all the items in the list
	 * 
	 * @return total price of items
	 */
	public double getTotalPrice() {
		double totalPrice = 0;
		for (Item item : getItems()) {
			totalPrice += item.getPrice();
		}
		return totalPrice;
	}

	/**
	 * Print out the item details in console
	 */
	public void printItems() {
		System.out.println("====================");
		System.out.println(String.format("Your order has got %d item(s) in it", getItems().size()));
		
		int i = 0;
		for (Item item : getItems()) {
			System.out.println("Order Item Nr: "+ ++i);
			item.print();
			System.out.println();
		}
	}

}
