package testat1_model;

/**
 * @author Keerthikan
 *
 */
public class ServiceItem extends Item {

	// ---------------------------------------------------------
	// Attributes
	// ---------------------------------------------------------

	private double price;

	// ---------------------------------------------------------
	// Constructors
	// ---------------------------------------------------------

	public ServiceItem(String description) {
		super(description);
	}

	public ServiceItem(String description, double price) {
		super(description);
		setPrice(price);
	}

	// ---------------------------------------------------------
	// Getters / Setters
	// ---------------------------------------------------------
	public void setPrice(double price) {
		if (price <= 0)
			throw new IllegalArgumentException("Price must be positive");
		this.price = price;
	}

	// ---------------------------------------------------------
	// Override Methods
	// ---------------------------------------------------------

	@Override
	public double getPrice() {
		return price;
	}

	/**
	 * Print out the ServiceItem details in console
	 */
	@Override
	public void print() {
		super.print();
		System.out.println(String.format("Service Charge: Fr. %.2f", getPrice()));
	}
}
