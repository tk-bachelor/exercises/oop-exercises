package testat3_io;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public class ObjectStreamHelper {
	
	public static void serialize(String path, Object object) throws IOException{
		OutputStream fos = new FileOutputStream(path);
		try(ObjectOutputStream stream = new ObjectOutputStream(fos)){
			stream.writeObject(object);
		}
	}
	
	public static <T> T deserialize(String path) throws ClassNotFoundException, IOException{
		InputStream fis = new FileInputStream(path);
		try(ObjectInputStream stream = new ObjectInputStream(fis)){
			return (T) stream.readObject();
		}
	}
}
