package testat3_io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ProcessingStreamHelper {

	private final static Charset ENCODING = StandardCharsets.UTF_8;

	public static List<String> readAllLines(String path) throws IOException {
		try (BufferedReader buffReader = new BufferedReader(
				new InputStreamReader(new FileInputStream(path), "UTF-8"))) {

			List<String> lines = new ArrayList<>();
			String line = buffReader.readLine();
			while (line != null) {
				lines.add(line);
				line = buffReader.readLine();
			}
			return lines;
		}
	}

	private static String decodePath(String path) throws UnsupportedEncodingException {
		// Umlaut compatibile file path
		return URLDecoder.decode(ProcessingStreamHelper.class.getClassLoader().getResource(path).getPath(), ENCODING.name());
	}

	public static void writeIntoFile(String path, String contents) throws IOException {
		File file = Paths.get(path).toFile();
		try (Writer writer = new BufferedWriter(new FileWriter(file))) {
			writer.write(contents);
		}
	}
}
