package week10_lambda;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

public class QueryList<T> {
	private Collection<T> coll;

	public QueryList() {

	}

	public QueryList(Collection<T> coll) {
		this.coll = coll;
	}

	public List<T> search(Predicate<T> criterion) {
		List<T> result = new ArrayList<>();
		
		coll.forEach( item -> {
			if(criterion.test(item))	
				result.add(item);
		});
		
		return result;
	}

	public double average(Function<T, Integer> criterion) {
		Iterator<T> it = coll.iterator();
		double total = 0;
		while (it.hasNext()) {
			total += criterion.apply(it.next());
		}
		return total / coll.size();
	}

	public T maximum(Function<T, Integer> criterion) {
		Iterator<T> it = coll.iterator();
		T maxObj = null;
		int maxVal = 0;
		while (it.hasNext()) {
			T item = it.next();
			if (criterion.apply(item) > maxVal) {
				maxVal = criterion.apply(item);
				maxObj = item;
			}
		}
		return maxObj;
	}

	public T minimum(Function<T, Integer> criterion) {
		Iterator<T> it = coll.iterator();
		T minObj = null;
		int minVal = 0;
		while (it.hasNext()) {
			T item = it.next();
			if (criterion.apply(item) < minVal) {
				minVal = criterion.apply(item);
				minObj = item;
			}
		}
		return minObj;
	}

	public <U extends Comparable<U>> boolean ascendinglySorted(Function<T, U> criterion) {
		Iterator<T> it = coll.iterator();
		T previous = null;
		while (it.hasNext()) {
			T item = it.next();

			if (previous != null) {
				U currentComparable = criterion.apply(item);
				U previousComparable = criterion.apply(previous);

				// -1, 0 means sorted ASC
				// 1 means sorted DESC
				if (previousComparable.compareTo(currentComparable) > 0) {
					previous.toString();
					currentComparable.toString();
					return false;
				}
			}
			previous = item;
		}
		return true;
	}
	
	public <U extends Comparable<U>> boolean descendinglySorted(Function<T, U> criterion) {
		Iterator<T> it = coll.iterator();
		T previous = null;
		while (it.hasNext()) {
			T item = it.next();
			
			if (previous != null) {
				U currentComparable = criterion.apply(item);
				U previousComparable = criterion.apply(previous);

				// -1 means sorted ASC
				// 0, 1 means sorted DESC
				if (previousComparable.compareTo(currentComparable) < 0) {
					previous.toString();
					currentComparable.toString();
					return false;
				}
			}
			previous = item;
		}
		return true;
	}
}
