package week10_lambda;

public class PersonUtil {

	public static int compareByAge(Person p1, Person p2) {
		return p1.getAge() - p2.getAge();
	}

	public static int compareByAgeDesc(Person p1, Person p2) {
		return p2.getAge() - p1.getAge();
	}

	public static int compareByLastname(Person p1, Person p2) {
		return p1.getLastName().compareTo(p2.getLastName());
	}

	public static int compareByNameLength(Person p1, Person p2) {
		String strP1 = p1.getFirstName() + p1.getLastName();
		String strP2 = p2.getFirstName() + p2.getLastName();

		return strP1.length() - strP2.length();
	}

	public static int compareByPlaceAndName(Person p1, Person p2) {
		int compared = p1.getCity().compareTo(p2.getCity());
		if (compared == 0) {
			compared = p1.getLastName().compareTo(p2.getLastName());

			if (compared == 0) {
				compared = p1.getFirstName().compareTo(p2.getFirstName());
			}
		}
		return compared;
	}

	public static int compareByNameAndAgeDesc(Person p1, Person p2) {
		int compared = p1.getLastName().compareTo(p2.getLastName());
		if (compared == 0) {
			compared = p2.getAge() - p1.getAge();
		}
		return compared;
	}
}
