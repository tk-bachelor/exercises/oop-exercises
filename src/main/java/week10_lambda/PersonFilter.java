package week10_lambda;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;

public class PersonFilter {
	
	public static <T> List<T> search(List<T> input , Predicate<T>  criterion){
		 Iterator<T> it = input.iterator();
		 List<T> output = new ArrayList<T>();
		 while(it.hasNext()){
			 T t = it.next();
			 if(criterion.test(t)){
				 output.add(t);
			 }
		 }
		 return output;
	}
}
