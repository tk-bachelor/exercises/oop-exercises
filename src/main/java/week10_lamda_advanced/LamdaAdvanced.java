package week10_lamda_advanced;

import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class LamdaAdvanced {

	
	public static <T> void printPersonWithPredicate(
			List<T> list, Predicate<T> predicator,
			Consumer<T> consumer){
		Iterator<T> it = list.iterator();
		
		while (it.hasNext()) {
			T item = it.next();
			if (predicator.test(item)) {
				consumer.accept(item);
			}
		}
	}
}
