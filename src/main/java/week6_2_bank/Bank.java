package week6_2_bank;

public class Bank {

	public void transfer(BankAccount from, BankAccount to, int amount) throws TransactionException {
		if (amount < 0) {
			throw new TransactionException("Invalid negative amount");
		}

		// check if transaction is possible by sender
		if (!from.isValidWithdrawal(amount)) {
			throw new TransactionException("Transaction Failure: Sender is not eligible to send this amount");
		}

		// check if transaction is possible to recipient
		if (!to.isValidDeposit(amount)) {
			throw new TransactionException("Transaction Failure: Recipient is not eligible to receive this amount");
		}

		// withdraw
		try {
			from.withdraw(amount);
		} catch (Exception ex) {
			throw new TransactionException(ex.getMessage());
		}

		// deposit
		try {
			to.deposit(amount);
		} catch (Exception ex) {
			// return money
			try {
				from.deposit(amount);
			} catch (LimitExceededException e) {
				throw new TransactionException("Amount couldn't be returned to sender");
			}
			throw new TransactionException("Amount couldn't be sent to recipient");
		}
	}
}
