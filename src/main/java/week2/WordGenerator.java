package week2;

/**
 * @author Keerthikan
 * 
 */
public class WordGenerator {
	public static void main(String[] args) {
		
		int worldLength = 4;
		generateWord("", worldLength);	
		
	}
	
	public static void generateWord(String text, int charAt){
		for(char c='A'; c<='Z'; c++){
			if(charAt == 1){
				System.out.println(text + c);
			}else{
				generateWord(text + c, charAt-1);
			}
		}
	}
}
