package week2;

import java.math.BigInteger;

/**
 * 
 * @author Keerthikan
 * W2 E2: Recursive Double Factorial
 */
public class RecursiveDoubleFactorial {

	public static void main(String[] args) {

		// factorial number
		int number = 10000;
		long startTime = System.currentTimeMillis();
		BigInteger factorial = doubleFactorial(number);
		
		long stopTime = System.currentTimeMillis();
		System.out.println("Double Factorial of " + number + " is " + factorial.toString());
		System.out.println("Calculated Time : " + (stopTime-startTime));
	}

	private static BigInteger doubleFactorial(int number) {
		if (number == 1) {
			return BigInteger.ONE;
		} else if (number == 2) {
			return BigInteger.valueOf(2);
		} else {
			BigInteger product = BigInteger.valueOf(number);
			return product = product.multiply(doubleFactorial(number - 2));
		}
	}
}
