package week2;

/**
 * 
 * @author Keerthikan <br/>
 *         Exercise 6 - Type Conversion <br/>
 *         Exercise 7 - Numeric Extension <br/>
 *         Exercise 8 - Overloading
 */
public class TypeConversion {

	public static void main(String[] args) {

		convertTypes();
		calcNumbers();
		overloading();
	}

	// Exercise 6
	private static void convertTypes() {
		char c = '!';
		byte b = 127;
		short s = 1;
		int i = 3;
		long l = 4;
		float f = 0.5f;
		double d = 0.6;

		// Type conversion

		long z = b;
		int a = (int) (f * 2);
		float x = 0x12345678; // hex
		double y = l * l;

		z = (long) (y * y);

		// byte + 1 -> int
		s = (short) (b + 1);
		b = (byte) (c + 1);
		char c2 = (char) (c + 1);
		b += 1;
	}

	// Exercise 7
	private static void calcNumbers() {
		int i = 1;
		long l = 2;
		float f = .1f;
		double d = 0.2;

		// a
		float x1 = 1 / 4; // 1/4 -> 0.0
		System.out.println(x1);
		// 1/4 is a division with integer numbers. Therefore 0.25 will be 0.0.
		// correction: (float) 1/4.0
		x1 = (float) (1 / 4.0);

		// b
		double x2 = i / 4.0; // 1/4 -> 0.25
		System.out.println(x2);

		// c
		float x3 = 1.0f / l; // 1.0/2 -> 0.5
		System.out.println(x3);

		// d
		double y1 = 123456789 + f;
		System.out.println(y1); // 1.23456792E8

		// e
		double y2 = 123456789.0 + f;
		System.out.println(y2); // 1.234567891E8

		// f
		long z1 = Integer.MAX_VALUE + 1;
		System.out.println(z1); // overflow -> Integer.MIN_VALUE (negative)

		long z2 = Integer.MAX_VALUE * 2;
		System.out.println(z2); // MAX_VALUE + MAX_VALUE = -2 (overflow)

		// float z3 = i / 0; // Arithmetic Exception
	}

	// Exercise 8
	private static void overloading() {
		int a = calculate((int) 2.0); // compile error
		float b = calculate(1, 2.0f);
		float c = calculate((float) 1, 2); // compile error
		double d = calculate(1.0f, 2.0f); // float will be converted to double
		double e = calculate(1.0, 2.0f); // double double
		double f = calculate(1L, 2); // long will become float
	}

	static int calculate(int i) {
		System.out.println("int");
		return i;
	}

	static float calculate(int x, float y) {
		System.out.println("int float");
		return x + y;
	}

	static float calculate(float x, int y) {
		System.out.println("float int");
		return x + y;
	}

	static double calculate(double x, double y) {
		System.out.println("double double");
		return x + y;
	}
}
