package week2;

import java.util.Arrays;

public class KnightsTour {
	private static final int NOF_ROWS = 6;
	private static final int NOF_COLS = 6;

	private static final int FREE = 0;

	private final int[][] board = new int[NOF_ROWS][NOF_COLS]; // FREE == 0 for
																// unvisited
	private final int[][] moves = { { -1, -2 }, { -1, +2 }, { +1, -2 },
			{ +1, +2 }, { -2, -1 }, { -2, +1 }, { +2, -1 }, { +2, +1 } };

	private boolean findTour(int x, int y, int step) {
		if (insideBoard(x, y)) {
			if (step == NOF_ROWS * NOF_COLS) {
				return board[x][y] == 1; // closed tour
			} else if (board[x][y] == FREE) {
				board[x][y] = step + 1;
				int[][] next = nextPositions(x, y);
				for (int k = 0; k < next.length; k++) {
					if (findTour(next[k][0], next[k][1], step + 1)) {
						return true;
					}
				}
				board[x][y] = FREE;
			}
		}
		return false;
	}

	private int[][] nextPositions(int x, int y) {
		int[][] next = new int[moves.length][];
		for (int k = 0; k < moves.length; k++) {
			next[k] = new int[] { x + moves[k][0], y + moves[k][1] };
		}
		// Warnsdorff rule: select moves first if reachable from less free squares
		Arrays.sort(next, (left, right) -> {
			int leftCount = countReachables(left[0], left[1]);
			int rightCount = countReachables(right[0], right[1]);
			return leftCount - rightCount;
		});
		return next;
	}

	private int countReachables(int x, int y) {
		int count = 0;
		for (int[] move : moves) {
			int nx = x + move[0];
			int ny = y + move[1];
			if (insideBoard(nx, ny) && board[nx][ny] == FREE) {
				count++;
			}
		}
		return count;
	}

	private boolean insideBoard(int x, int y) {
		return x >= 0 && x < NOF_ROWS && y >= 0 && y < NOF_COLS;
	}

	private void printTour() {
		for (int x = 0; x < NOF_ROWS; x++) {
			for (int y = 0; y < NOF_COLS; y++) {
				System.out.print(board[x][y] + "\t");
			}
			System.out.println();
		}
	}

	public static void findTour() {
		KnightsTour tour = new KnightsTour();
		if (tour.findTour(1, 0, 0)) {
			tour.printTour();
		} else {
			System.out.println("No knight tour");
		}
	}

	public static void main(String[] args) {
		findTour();
	}
}
