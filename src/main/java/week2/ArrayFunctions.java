package week2;

/**
 * 
 * @author Keerthikan
 * Week 2 - Exercise 1: Array Functions
 */
public class ArrayFunctions {
	
	public static void main(String[] args) {
		
		// numbers to be reverted
		int[] numbers = {1,2,3,4,5,6,7,8,9,10};
		int[] revertedNumbers = revert(numbers);
		print("Reverted", revertedNumbers);
		
		// Ascending Sorted
		int[] numbers2 = {3,1,4,2,4,1,5,4,61,6};
		boolean isNumbersASC = ascendingOrdered(numbers);
		boolean isNumbers2ASC = ascendingOrdered(numbers2);
		print("Numbers 1 Ascending - " + isNumbersASC, numbers);
		print("Numbers 2 Ascending - " + isNumbers2ASC, numbers2);
		
		// Descending Sorted
		int[] numbers3 = {9,6,5,4,3,2,-1,-3,-5};
		boolean isNumbersDESC = descendingOrdered(numbers);
		boolean isNumbers3DESC = descendingOrdered(numbers3);
		print("Numbers 1 DESC - " + isNumbersDESC, numbers);
		print("Numbers 3 DESC - " + isNumbers3DESC, numbers3);
	}
	
	public static int[] revert(int[] numbers){
		int length = numbers.length;
		int[] reverted = new int[length];
		
		for(int i = numbers.length-1; i>=0; i--){
			reverted[length - i - 1] = numbers[i];
		}
		return reverted;
	}
	
	public static boolean ascendingOrdered(int[] numbers){
		for(int i=0; i<numbers.length;i++){
			// the end of array
			if(i+1 == numbers.length)
				break;
			else if(numbers[i]>numbers[i+1]){
				return false;
			}				
		}
		
		return true;
	}
	
	public static boolean descendingOrdered(int[] numbers){
		for(int i=0; i<numbers.length;i++){
			// the end of array
			if(i+1 == numbers.length)
				break;
			else if(numbers[i]<numbers[i+1]){
				return false;
			}				
		}
		
		return true;
	}
	
	public static void print(String text, int[] numbers){
		System.out.print(text + ": ");
		for(int value : numbers){
			System.out.print(value + " ");
		}
		System.out.println();
	}
}
