package week2;

import java.util.Random;

/**
 * 
 * @author Keerthikan
 * W2 E2: Recursive Sum
 */
public class RecursiveSum {
	public static void main(String[] args) {
		int[] series = randomSeries(10000);
		int[] series2 = {1,2,3,4,5};

		// Compute the sum of the series recursively
		long summand = sum(series2, 0);
		System.out.println(summand);
	}

	static long sum(int[] series, int index) {
		if (index == series.length - 1)
			return series[index];
		else {
			long summand = series[index] + sum(series, index + 1);
			return summand;
		}
	}

	static int[] randomSeries(int amount) {
		Random random = new Random(4711);
		int[] series = new int[amount];
		for (int index = 0; index < amount; index++) {
			series[index] = random.nextInt();
		}
		return series;
	}
}
