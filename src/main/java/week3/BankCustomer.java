package week3;

import java.util.ArrayList;
import java.util.List;

public class BankCustomer {
	private String firstname;
	private String lastname;
	private String address;
	private int age;
	
	private BankManager manager;
	private List<BankAccount> accounts;
	
	public BankCustomer(String firstname, String lastname, String address, int age){
		this.firstname = firstname;
		this.lastname = lastname;
		this.address = address;
		this.age = age;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public String getAddress() {
		return address;
	}

	public int getAge() {
		return age;
	}

	public BankManager getManager() {
		return manager;
	}

	public List<BankAccount> getAccounts() {
		if(accounts == null)
			this.accounts = new ArrayList<BankAccount>();
		return accounts;
	}

	public void setManager(BankManager manager) {
		this.manager = manager;
	}
	
	public void print() {
		System.out.println("\t-------------------------------");
		System.out.println(String.format("\tCustomer: %s %s, %d, %s", firstname, lastname, age, address));
		System.out.println("\tAccounts: "+getAccounts().size());
		for(BankAccount account : accounts){
			account.print();
		}
		System.out.println();
	}
	
	public BankAccount openNewAccount(long number){
		BankAccount account = new BankAccount(number);
		account.setCustomer(this);
		getAccounts().add(account);
		return account;
	}
	
}
