package week3;

public class Locomotive {
	private String name;

	public Locomotive(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

}
