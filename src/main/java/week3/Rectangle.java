package week3;

import java.awt.Point;

public class Rectangle {
	private Point topLeft;
	private Point bottomRight;

	public Rectangle(Point topLeft, Point bottomRight) {
		this.topLeft = topLeft;
		this.bottomRight = bottomRight;
	}

	public Rectangle(Point topLeft, int size) {
		this.topLeft = topLeft;
		this.bottomRight = new Point(this.topLeft.x + size, topLeft.y + size);
	}

	public Point getTopLeft() {
		return topLeft;
	}

	public Point getBottomRight() {
		return bottomRight;
	}

	public boolean isSquare() {
		return getHeight() == getWidth();
	}

	private int getHeight() {
		return bottomRight.y - topLeft.y;
	}

	private int getWidth() {
		return bottomRight.x - topLeft.x;
	}

	public boolean isSame(Rectangle other) {
		return this.topLeft.x == other.topLeft.x && this.topLeft.y == other.topLeft.y
				&& this.bottomRight.x == other.bottomRight.x && this.bottomRight.y == other.bottomRight.y;
	}

	public boolean encloses(Rectangle other) {
		if (isSame(other)) {
			return true;
		} else {
			return other.topLeft.x >= this.topLeft.x && other.topLeft.y >= this.topLeft.y
					&& other.bottomRight.x <= this.bottomRight.x && other.bottomRight.y <= this.bottomRight.y;
		}
	}

	public boolean overlaps(Rectangle other) {
		// is other inside this
		if (encloses(other))
			return true;

		// if this inside other
		if (other.encloses(this))
			return true;

		// other points are inside this
		return isEdgeInside(other.topLeft) || isEdgeInside(other.bottomRight) || isEdgeInside(other.getBottomLeft())
				|| isEdgeInside(other.getTopRight());

	}

	private boolean isEdgeInside(Point edge) {
		return edge.x > this.topLeft.x && edge.x < this.bottomRight.x && edge.y > this.topLeft.y
				&& edge.y < this.bottomRight.y;

	}

	public Point getTopRight() {
		return new Point(bottomRight.x, topLeft.y);
	}

	public Point getBottomLeft() {
		return new Point(topLeft.x, bottomRight.y);
	}

	public Rectangle strech(int factor){
		int strechedWidth = getWidth()* factor;
		int strechedHeight = getHeight() * factor;
		
		Point topLeftNew = new Point(this.topLeft);
		Point strechedBottomRight = new Point(topLeft.x+strechedWidth, topLeft.y+strechedHeight);
		Rectangle r = new Rectangle(topLeftNew, strechedBottomRight);
		return r;
	}
	
	public Rectangle shrink(int factor){
		int shrinkedWidth = getWidth() / factor;
		int shrinkedHeight = getHeight() / factor;
		
		Point topLeftNew = new Point(this.topLeft);
		Point shrinkedBottomRight = new Point(topLeft.x+shrinkedWidth, topLeft.y+shrinkedHeight);
		Rectangle r = new Rectangle(topLeftNew, shrinkedBottomRight);
		return r;
	}
}
