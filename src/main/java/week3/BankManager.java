package week3;

import java.util.ArrayList;
import java.util.List;

public class BankManager {
	private String firstname;
	private String lastname;
	private int managerID;
	
	private List<BankCustomer> customers;
	
	public BankManager(String firstname, String lastname, int managerId){
		this.firstname = firstname;
		this.lastname = lastname;
		this.managerID = managerId;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public int getManagerID() {
		return managerID;
	}

	public List<BankCustomer> getCustomers() {
		if(customers == null)
			customers = new ArrayList<BankCustomer>();
		return customers;
	}
	
	public void addCustomer(BankCustomer customer){
		customer.setManager(this);
		this.getCustomers().add(customer);
	}
	
	public void removeCustomer(BankCustomer customer){
		this.getCustomers().remove(customer);
	}
	
	public void print() {
		System.out.println("-------------------------------");
		System.out.println(String.format("Manager: %s %s, %d", firstname, lastname, managerID));
		System.out.println("Customers: "+getCustomers().size());
		for(BankCustomer customer : customers){
			customer.print();
		}
		System.out.println();
	}
}
