package week3;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Train {
	
	private Locomotive locomotive;
	private List<Car> cars;
	
	public Train(){
		locomotive = new Locomotive("Locomotive");
	}
	
	public Locomotive getLocomotive() {
		return locomotive;
	}

	public List<Car> getCars(){
		if(cars == null)
			cars = new LinkedList<Car>();
		return cars;
	}
	
	public void add(String name){
		getCars().add(new  Car(name));
	}
	
	public int nrOfCars(){
		return getCars().size();
	}
	
	public String getCarName(int position){
		Car car = getCars().get(position);
		if(car == null){
			return "";
		}
		return car.getName();
	}
	
	public void insert(int position, String name){
		getCars().set(position, new Car(name));
	}
	
	public Car removeFirst(){
		return getCars().remove(0);
	}
	
	public void revert(){
		Collections.reverse(getCars());
	}
	
	public void print(){
		System.out.println("==================");
		System.out.println("Train");
		System.out.println();
		
		System.out.print("<==");
		System.out.print(locomotive.getName());
		System.out.print(" -==- ");
		
		// Cars
		for(Car car: getCars())
			System.out.print(String.format(" %s -==-", car.getName()));
		
	}
	
}
