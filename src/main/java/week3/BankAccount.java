package week3;

/**
 * 
 * @author Keerthikan Week 3 - Object Oriented Programming
 */

public class BankAccount {
	private long accountNumber;
	private double balance;
	public String pub = "test";
	protected String protect = "protected";

	private BankCustomer customer;

	public BankAccount(long accountNumber) {
		this.accountNumber = accountNumber;
		this.balance = 0;
	}

	public long getAccountNumber() {
		return accountNumber;
	}

	public double getBalance() {
		return balance;
	}

	public boolean deposit(double amount) {
		if (amount < 0) {
			return false;
		}
		balance += amount;
		return true;
	}

	public boolean withdraw(double amount) {
		if (balance < amount || amount < 0) {
			return false;
		}

		balance -= amount;
		return true;
	}

	public BankCustomer getCustomer() {
		return customer;
	}

	public void setCustomer(BankCustomer customer) {
		this.customer = customer;
	}
	
	public boolean sameCustomer(BankAccount other){
		return this.customer == other.getCustomer();			
	}
	
	public BankManager getManager() {
		return getCustomer().getManager();		
	}
	
	public void print() {
		System.out.println("\t\t-------------------------------");
		System.out.println("\t\tBank-Account: " + accountNumber);
		System.out.println("\t\tBalance: "+balance);
		System.out.println();
	}
}
