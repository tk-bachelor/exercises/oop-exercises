package week7_5_shuffle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Shuffler {
	
	private int loopCounter = 0;

	@Deprecated
	public int[] randomSeries(int amount) {
		int[] result = new int[amount];

		int i = 0;
		do {
			randomFill(result);
			++i;
		} while (hasDuplicates(result));
		System.out.println("Deprecated Method: " + i);
		return result;
	}

	public int[] randomSeriesNew(int amount) {
		int[] result = new int[amount];

		randomFill(result);
		while (hasDuplicates(result)) {
			// replace duplicates
			replaceDuplicates(result);
		}
		System.out.println("New Method: " + loopCounter);
		return result;
	}

	private void randomFill(int[] array) {
		Random random = new Random();
		for (int index = 0; index < array.length; index++) {
			array[index] = 1 + random.nextInt(array.length);
		}
	}

	private void replaceDuplicates(int[] array) {
		List<Integer> distinct = getList(array);

		// go through list and replace duplicates
		for (int i = 0; i < distinct.size() - 1; i++) {
			int value = distinct.get(i);

			// sublist = rest of the list ahead
			List<Integer> sublist = distinct.subList(i + 1, distinct.size());

			// duplicate check
			int firstDuplicateIndex = sublist.indexOf(value);

			// no duplicates
			if (firstDuplicateIndex == -1)
				continue;

			// get new unique value
			int newValue = getNewValue(distinct);
			distinct.set(i, newValue);
			array[i] = newValue;
		}
	}

	private int getNewValue(List<Integer> origin) {
		Random random = new Random();
		int newValue;
		do {
			newValue = random.nextInt(origin.size());
			loopCounter++;
		} while (origin.contains(newValue));
		return newValue;
	}

	private boolean hasDuplicates(int[] array) {
		int[] sorted = array.clone();
		Arrays.sort(sorted);
		for (int index = 0; index < array.length - 1; index++) {
			if (sorted[index] == sorted[index + 1]) {
				return true;
			}
		}
		return false;
	}

	private List<Integer> getList(int[] array) {
		List<Integer> intList = new ArrayList<>();
		for (int index = 0; index < array.length; index++)
			intList.add(array[index]);
		return intList;
	}
}
