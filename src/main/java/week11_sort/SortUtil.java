package week11_sort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class SortUtil {
	
	public static void bubbleSort(long[] array){
		boolean swapped;
		int count = 0;
		do{
			swapped = false;
			for(int i = 0; i < array.length - 1 ; i++){
				if(array[i] > array[i + 1]){
					swap(array, i, i+1);
					swapped = true;
				}
			}
			System.out.println(count);
		}while(swapped);
	}
	
	public static void quickSort(long[] array, int start, int end){
		long pivot = array[(start + end)/2];

		int i = start;
		int j = end;
		
		while(i <= j){
			while(array[i] < pivot) {i++;}
			while(array[j] > pivot) {j--;}
			
			if(i <= j){
				swap(array, i, j);
				i++;
				j--;
			}
		}
		if(start < j){
			quickSort(array, start, j);
		}
		if(i < end) {
			quickSort(array, i, end);
		}
	}
	
	public static void treeSort(long[] array){
		Set<Long> set = new TreeSet<Long>();
		for(long l : array){
			set.add(l);
		}
		
		int index = 0;
		for(long l : set){
			array[index++] = l;
		}
	}
	

	private static void swap(long[] array, int i, int j) {
		long temp = array[i];
		array[i] = array[j];
		array[j] = temp;		
	}
	
	
}
