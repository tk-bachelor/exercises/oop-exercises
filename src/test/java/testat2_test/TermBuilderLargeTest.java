package testat2_test;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;

import testat2_builder.ModuleBuilder;
import testat2_builder.TermBuilder;
import testat2_builder.TermPrerequisiteBuilder;
import testat2_builder.TermSuccessorBuilder;
import testat2_studienplan.CatalogueReader;
import testat2_studienplan.Module;
import testat2_studienplan.Term;

public class TermBuilderLargeTest {

	// Builder with file data
	public static ModuleBuilder builder;

	@BeforeClass
	public static void before() throws FileNotFoundException, UnsupportedEncodingException, Exception {
		try (CatalogueReader reader = new CatalogueReader("LargeCatalogue.txt")) {
			Map<String, String[]> data = reader.readAll();

			assertEquals("Number of lines", 1000, data.size());
			builder = new ModuleBuilder(data);
		}
	}

	@Test
	public void testBuildStudyPlan() {
		System.out.println("Prerequisites:");

		// Stopwatch - Module Builder
		long moduleBuilderStart = System.currentTimeMillis();
		Set<Module> modules = builder.buildPrerequisites();

		System.out.println("ModuleBuilder: Time elapsed : " + (System.currentTimeMillis() - moduleBuilderStart) + "ms");
		assertEquals("Number of modules", 1000, modules.size());

		TermPrerequisiteBuilder termBuilder = new TermPrerequisiteBuilder();

		// Stopwatch - Term Builder
		long start = System.currentTimeMillis();

		// build study plan
		Set<Term> terms = termBuilder.buildStudyPlan(modules);
		System.out.println("Term Builder: Time elapsed : " + (System.currentTimeMillis() - start) + "ms");

		// Average
		// Module Builder: 1700 - 1800 ms
		// Term Builder: 90 - 120ms
	}

	@Test
	public void testBuildStudyPlanSuccessor() {
		System.out.println("Successors:");

		// Stopwatch - Module Builder
		long moduleBuilderStart = System.currentTimeMillis();
		Set<Module> modules = builder.buildSuccessors();

		System.out.println("ModuleBuilder: Time elapsed : " + (System.currentTimeMillis() - moduleBuilderStart) + "ms");
		assertEquals("Number of modules", 1000, modules.size());

		TermBuilder termBuilder = new TermSuccessorBuilder();

		// Stopwatch
		long start = System.currentTimeMillis();
		// build study plan
		Set<Term> terms = termBuilder.buildStudyPlan(modules);
		System.out.println("TermBuilder: Time elapsed : " + (System.currentTimeMillis() - start) + "ms");

		// Average
		// Module Builder: 1450 - 1600 ms
		// Term Builder: 90 - 100ms
	}

}
