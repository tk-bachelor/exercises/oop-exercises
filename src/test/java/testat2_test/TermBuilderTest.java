package testat2_test;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;

import testat2_builder.ModuleBuilder;
import testat2_builder.TermBuilder;
import testat2_builder.TermPrerequisiteBuilder;
import testat2_builder.TermSuccessorBuilder;
import testat2_studienplan.CatalogueReader;
import testat2_studienplan.Module;
import testat2_studienplan.Term;

public class TermBuilderTest {
	
	// Builder with file data
	public static ModuleBuilder builder;
	
	@BeforeClass
	public static void before() throws FileNotFoundException, UnsupportedEncodingException, Exception{
		try (CatalogueReader reader = new CatalogueReader("StudyCatalogue.txt")) {
			Map<String, String[]> data = reader.readAll();
			
			assertEquals("Number of lines", 11, data.size());
			builder = new ModuleBuilder(data);
		}
	}
	
	@Test
	public void testBuildStudyPlan() {
		System.out.println("Prerequisites:");
		
		long moduleBuilderStart = System.currentTimeMillis();
		Set<Module> modules = builder.buildPrerequisites();
		System.out.println("ModuleBuilder: Time elapsed : " + (System.currentTimeMillis() - moduleBuilderStart)+"ms");
		
		assertEquals("Number of modules", 11, modules.size());
		
		TermBuilder termBuilder = new TermPrerequisiteBuilder();
		
		// Stopwatch
		long start = System.currentTimeMillis();
		// build study plan
		Set<Term> terms = termBuilder.buildStudyPlan(modules);
		System.out.println("Prerequisites: Time elapsed : " + (System.currentTimeMillis() - start)+"ms");
		
		assertEquals("Number of terms", 5, terms.size());
		
		// Print Terms
		terms.forEach(term ->{
			term.print();
		});
		System.out.println();
	}
	
	@Test
	public void testBuildStudyPlanSuccessor() {
		System.out.println("Successors:");
		
		// Stopwatch
		long moduleBuilderStart = System.currentTimeMillis();
		Set<Module> modules = builder.buildSuccessors();
		System.out.println("ModuleBuilder: Time elapsed : " + (System.currentTimeMillis() - moduleBuilderStart)+"ms");
		
		assertEquals("Number of modules", 11, modules.size());
		
		TermBuilder termBuilder = new TermSuccessorBuilder();
		
		// Stopwatch
		long start = System.currentTimeMillis();
		// build study plan
		Set<Term> terms = termBuilder.buildStudyPlan(modules);
		System.out.println("TermBuilder: Time elapsed : " + (System.currentTimeMillis() - start)+"ms");
		
		assertEquals("Number of terms", 5, terms.size());
		
		// Print Terms
		terms.forEach(term ->{
			term.print();
		});
	}

}
