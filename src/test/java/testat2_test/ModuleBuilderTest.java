package testat2_test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;

import testat2_builder.ModuleBuilder;
import testat2_studienplan.CatalogueReader;
import testat2_studienplan.Module;

public class ModuleBuilderTest {
	
	// Builder with file data
	public static ModuleBuilder builder;
	
	@BeforeClass
	public static void before() throws FileNotFoundException, UnsupportedEncodingException, Exception{
		try (CatalogueReader reader = new CatalogueReader("StudyCatalogue.txt")) {
			Map<String, String[]> data = reader.readAll();
			
			assertEquals("Number of lines", 11, data.size());
			builder = new ModuleBuilder(data);
		}
	}
	
	@Test
	public void testBuildPrerequisites() {
		Set<Module> modules = builder.buildPrerequisites();
		assertEquals("Number of modules", 11, modules.size());
		
		// Test Module SE1
		Module se1 = builder.findByName(modules, "SE1");
		assertNotNull("Software Engineering 1 is empty", se1);
		
		// Prerequisites
		Set<Module> prereqs = se1.getModules();
		assertEquals("3 Prerequisite modules for SE1", 3, prereqs.size());
		
		// Prereqs are AD1, CP1, DB1
		String[] expected = {"AD1", "CPI", "DB1"};
		
		// check prerequisite modules explicitely
		prereqs.forEach(module -> {
			boolean contains = false;
			for(String moduleName : expected){
				if(module.getName().equals(moduleName)){
					contains = true;
					break;
				}
			}
			assertTrue(contains);
		});
	}
	
	@Test
	public void testBuildSuccessors() {
		Set<Module> modules = builder.buildSuccessors();
		assertEquals("Number of modules", 11, modules.size());
		
		// Test Module SE1
		Module se1 = builder.findByName(modules, "SE1");
		assertNotNull("Software Engineering 1 is empty", se1);
		
		// Prerequisites
		assertEquals("3 Prerequisite modules for SE1", 3, se1.getTotalPrerequisites());
		
		// Successors
		Set<Module> successors = se1.getModules();
		assertEquals("1 successor module for SE1", 1, se1.getModules().size());
		
		
		// Prereqs are AD1, CP1, DB1
		String[] expected = {"SE2"};
		
		// check prerequisite modules explicitely
		successors.forEach(module -> {
			boolean contains = false;
			for(String moduleName : expected){
				if(module.getName().equals(moduleName)){
					contains = true;
					break;
				}
			}
			assertTrue(contains);
		});
	}

}
