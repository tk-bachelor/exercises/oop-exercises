package week3;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class BankSystemTest {

	private BankManager manager1;
	private BankManager manager2;

	@Before
	public void setup() {
		createManager1();
		createManager2();
	}

	private void createManager1() {
		// Manager 1
		manager1 = new BankManager("Kate", "Smith", 1);

		// Customers
		BankCustomer klara = new BankCustomer("Klara", "Meier", "No idea", 30);
		BankCustomer petra = new BankCustomer("Petra", "M�ller", "Somewhere", 20);

		// Add to customers list
		manager1.addCustomer(klara);
		manager1.addCustomer(petra);

		// Bank Accounts of Klara
		BankAccount account1 = klara.openNewAccount(1);
		account1.deposit(12000);
		BankAccount account2 = klara.openNewAccount(2);
		account2.deposit(1000);

		// Bank Accounts for Petra
		BankAccount account3 = petra.openNewAccount(3);
		account3.deposit(5000);
	}

	private void createManager2() {
		// Manager 2
		manager2 = new BankManager("Uwe", "Schnell", 2);

		// Customers
		BankCustomer hans = new BankCustomer("Hans", "Meier", "Meienfeld 3", 24);
		BankCustomer stefan = new BankCustomer("Stefan", "Schmid", "Schmidhof", 22);

		// Add to customers list
		manager2.addCustomer(hans);
		manager2.addCustomer(stefan);

		// Bank Accounts of Hans
		hans.openNewAccount(4);

		// Bank Accounts for Stefan
		BankAccount account5 = stefan.openNewAccount(5);
		account5.deposit(6000);

	}

	 @Test
	 public void testManager1(){
		 assertTrue(manager1.getCustomers().size()==2);
		 
//		 manager1.print();
	 }
	 
	 @Test
	 public void testManager2(){
		 assertTrue(manager2.getCustomers().size()==2);
		 
//		 manager2.print();
	 }
}
