package week3;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Before;
import org.junit.Test;

public class BankAccountTest{
	
	private BankAccount account;
	
	@Before
	public void setup(){
		account = new BankAccount(341515);
	}

	@Test
	public void testDeposit(){
		
		// deposit 50.-
		account.deposit(50);
		assertEquals("50 Francs in balance", 50, account.getBalance(), 0.01);		
	}
	
	@Test
	public void testMinusDeposit(){
		
		// deposit -50.-
		boolean result = account.deposit(-50);
		assertFalse(result);
		assertEquals("0 Francs in balance", 0, account.getBalance(), 0.01);		
	}
	
	@Test
	public void testWithdraw(){
		
		// deposit 50.- and withdraw 20.50
		account.deposit(50);
		account.withdraw(20.50);
		assertEquals("29.50 Francs in balance", 29.50, account.getBalance(), 0.01);		
	}
	
	@Test
	public void testNegativeWithdraw(){
		
		// deposit 50.- and withdraw -50
		account.deposit(50);
		boolean result = account.withdraw(-50);
		assertFalse(result);
		assertEquals("50 Francs in balance", 50, account.getBalance(), 0.01);		
	}
	
	@Test
	public void testWithdrawNoBalance(){
		
		// withdraw 50
		boolean result = account.withdraw(50);
		assertFalse(result);
		assertEquals("0 Francs in balance", 0, account.getBalance(), 0.01);		
	}
	
	@Test
	public void testWithdrawNotEnoughBalance(){
		
		// deposit 50 and withdraw 100
		account.deposit(50);
		boolean result = account.withdraw(100);
		assertFalse(result);
		assertEquals("50 Francs in balance", 50, account.getBalance(), 0.01);		
	}
	
	
	
	
	
}
