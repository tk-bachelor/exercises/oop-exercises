package week3;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class TrainTest {

	private Train train;

	@Before
	public void setup() {
		train = new Train();
		
		train.add("Car 1");
		train.add("Car 2");
		train.add("Car 3");
	}


	 @Test
	 public void printTrain(){
		 train.print();
	 }
	 
}
