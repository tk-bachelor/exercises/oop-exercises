package week9;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import week9_pair.OrderedPair;
import week9_pair.Pair;
import week9_pair.UnorderedPair;

public class PairTest {
	@Test
	public void testOrderedPairGetters() {
		Pair<Integer> a = new OrderedPair<>(1, 2);

		assertEquals(new Integer(1), a.getFirst());
		assertEquals(new Integer(2), a.getSecond());
	}

	@Test
	public void testUnorderedPairGetters() {
		Pair<Integer> a = new UnorderedPair<>(1, 2);
		assertEquals(new Integer(1), a.getFirst());
		assertEquals(new Integer(2), a.getSecond());
	}

	@Test
	public void testOrderedPairClone() {
		Pair<Integer> a = new OrderedPair<>(1, 2);
		Pair<Integer> clone = a.clone();
		assertTrue(clone instanceof OrderedPair);
		assertEquals(a.getFirst(), clone.getFirst());
		assertEquals(a.getSecond(), clone.getSecond());
	}

	@Test
	public void testUnorderedPairClone() {
		Pair<Integer> a = new UnorderedPair<>(1, 2);
		Pair<Integer> clone = a.clone();
		assertTrue(clone instanceof UnorderedPair);
		assertEquals(a.getFirst(), clone.getFirst());
		assertEquals(a.getSecond(), clone.getSecond());
	}

	@Test
	public void testOrderedPairEquals() {
		Pair<Integer> a = new OrderedPair<>(1, 2);
		checkEquals(a, a);
		assertFalse(a.equals(null));
		Pair<Integer> b = new OrderedPair<>(1, 2);
		checkEquals(b, a);
	}

	@Test
	public void testUnorderedPairEquals() {
		Pair<Integer> a = new UnorderedPair<>(1, 2);
		checkEquals(a, a);
		assertFalse(a.equals(null));
		Pair<Integer> b = new UnorderedPair<>(1, 2);
		checkEquals(b, a);
		Pair<Integer> c = new UnorderedPair<>(2, 1);
		checkEquals(c, a);
	}

	@Test
	public void testInterPairEquals() {
		Pair<Integer> a = new OrderedPair<>(1, 2);
		Pair<Integer> b = new UnorderedPair<>(1, 2);
		checkDiffers(a, b);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOrderedPairNullFirst() {
		new OrderedPair<>(null, 1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOrderedPairNullSecond() {
		new OrderedPair<>(1, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testUnorderedPairNullFirst() {
		new UnorderedPair<>(null, 1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testUnorderedPairNullSecond() {
		new UnorderedPair<>(1, null);
	}

	@Test
	public void compareOrderedPair() {
		Pair<Integer> a = new OrderedPair<>(1, 2);
		Pair<Integer> b = new OrderedPair<>(1, 2);
		Pair<Integer> c = new OrderedPair<>(2, 1);
		Pair<Integer> d = new OrderedPair<>(1, 3);

		assertEquals(0, a.compareTo(b));
		assertTrue(a.compareTo(c) < 0);
		assertTrue(c.compareTo(a) > 0);

		assertTrue(a.compareTo(d) < 0);
		assertTrue(d.compareTo(a) > 0);
	}

	@Test
	public void compareUnorderedPair() {
		Pair<Integer> a = new UnorderedPair<>(1, 2);
		Pair<Integer> b = new UnorderedPair<>(1, 2);
		Pair<Integer> c = new UnorderedPair<>(2, 1);
		Pair<Integer> d = new UnorderedPair<>(2, 3);
		Pair<Integer> e = new UnorderedPair<>(4, 3);

		assertEquals(0, a.compareTo(b));
		assertEquals(0, a.compareTo(c));
		assertEquals(0, c.compareTo(a));

		assertEquals(0, a.compareTo(d));
		assertEquals(0, d.compareTo(a));

		assertTrue(a.compareTo(e) < 0);
		assertTrue(e.compareTo(a) > 0);
	}
	
	@Test
	public void compareOrderedAndUnorderedPair(){
		Pair<Integer> a = new UnorderedPair<>(1, 2);
		Pair<Integer> b = new OrderedPair<>(1, 2);
		
		assertTrue(a.compareTo(b) > 0);
		assertTrue(b.compareTo(a) < 0);
	}

	private void checkEquals(Object obj1, Object obj2) {
		assertTrue(obj1.equals(obj2));
		assertTrue(obj2.equals(obj1));
		assertEquals(obj1.hashCode(), obj2.hashCode());
	}

	private void checkDiffers(Object obj1, Object obj2) {
		assertFalse(obj1.equals(obj2));
		assertFalse(obj2.equals(obj1));
	}

}
