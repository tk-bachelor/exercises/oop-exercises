package week9;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import week9_generics.CollectionFunctions;
import week9_generics.Person;

public class CollectionFunctionsTest {
	private static final List<Integer> INTEGER_LIST = Arrays.asList(1, -3, 22, 7, 43, 212, -87, 12, 6, 99, -65, 2162, 0, -1, -3, 7, 12);
	private static final int MEDIAN_INTEGER = 7;
	private static final List<Integer> LARGEST_3_INTEGERS = Arrays.asList(2162, 212, 99);
	private static final List<Integer> SMALLEST_3_INTEGERS = Arrays.asList(-87, -65, -3);

	
	private static final List<Person> PEOPLE_LIST = Arrays.asList(
			new Person("Klaus", "Adam", createDate(1978, 4, 13),
					"Oberseestrasse 14", 8640, "Rapperswil", "Student", "male"),
			new Person("Clara", "M�ller", createDate(1978, 4, 13),
					"Oberseestrasse 14", 8640, "Rapperswil", "Student", "female"),
			new Person("Hans", "Meier", createDate(1980, 1, 2),
					"Waldweg 2", 8640, "Rapperswil", "Student", "male"),
			new Person("Petra", "Schneider", createDate(1970, 8, 8),
					"Bahnhofstrasse 1", 8001, "Z�rich", "Manager", "female"),
			new Person("Klaus", "Schnell", createDate(1982, 12, 10),
					"R�tistrasse", 8630, "R�ti", "Engineer", "male"),
			new Person("Peter", "Winter", createDate(1965, 3, 3),
					"Hinterweg 12", 5000, "Aarau", "Carpenter", "male"),
			new Person("Sandra", "Vogt", createDate(1972, 11, 30),
					"Altstadtweg 8", 8001, "Z�rich", "Banker", "female"),
			new Person("Markus", "Marxer", createDate(1976, 2, 5),
					"Hauptstrasse 7", 6002, "Luzern", "Baker", "male"),
			new Person("Anna", "Widmer", createDate(1980, 7, 7),
					"Albisstrasse", 8004, "Z�rich", "Teacher", "female"),
			new Person("Hans", "Steiner", createDate(1976, 10, 10),
					"Seeufer 232", 8640, "Rapperswil", "Student", "male"),
			new Person("Vreni", "Affolter", createDate(1981, 4, 30),
					"Altstadtweg 17", 4000, "Basel", "Student", "female"),
			new Person("Karl", "Widmer", createDate(1977, 7, 31),
					"Zentralstrasse 87", 8640, "Rapperswil", "Engineer", "male"),
			new Person("Franz", "Eberhard", createDate(1978, 1, 1),
					"Hauptstrasse 10", 8640, "Rapperswil", "Student", "male"),
			new Person("Hedi", "Z�rcher", createDate(1950, 3, 22),
					"Bergstrasse 98", 8001, "Z�rich", "Retired", "female"),
			new Person("Monika", "Wehrli", createDate(1967, 9, 17),
					"Stadtweg 1", 5000, "Aarau", "Entrepreneur", "female"),
			new Person("Hans", "Beeler", createDate(1982, 6, 11),
					"Hauptstrasse 121", 8630, "R�ti", "Student", "male"));
	private static final Person MEDIAN_PERSON = PEOPLE_LIST.get(4);
	private static final Collection<Person> LARGEST_2_PEOPLE = Arrays.asList(PEOPLE_LIST.get(13), PEOPLE_LIST.get(5)); 
	private static final Collection<Person> SMALLEST_2_PEOPLE = Arrays.asList(PEOPLE_LIST.get(0), PEOPLE_LIST.get(10)); 
	
	private static LocalDate createDate(int year, int month, int dayOfMonth) {
		return LocalDate.of(year, month, dayOfMonth);
	}
	
	@Test
	public void testMergeToList() {
		// Integer Test
		List<Integer> mergedInt = CollectionFunctions.mergeToList(LARGEST_3_INTEGERS, SMALLEST_3_INTEGERS);
		assertEquals("Merged Integer List", 6, mergedInt.size());
		assertEquals(new Integer(-65),mergedInt.get(4));
		
		// Person Test
		List<Person> mergedPers = CollectionFunctions.mergeToList(LARGEST_2_PEOPLE, SMALLEST_2_PEOPLE);
		assertEquals(4, mergedPers.size());
		assertEquals(mergedPers.get(2), PEOPLE_LIST.get(0));
	}
	
	@Test
	public void testMergeToSet() {
		// Integer Test
		Set<Integer> mergedInt = CollectionFunctions.mergeToSet(LARGEST_3_INTEGERS, SMALLEST_3_INTEGERS);
		assertEquals("Merged Integer Set", 6, mergedInt.size());
		
		// Person Test
		Set<Person> mergedPers = CollectionFunctions.mergeToSet(LARGEST_2_PEOPLE, SMALLEST_2_PEOPLE);
		assertEquals(4, mergedPers.size());
	}
	
	@Test
	public void testMerge(){
		// Integer Test
		List<Integer> targetList = new ArrayList<>();
		CollectionFunctions.merge(INTEGER_LIST, LARGEST_3_INTEGERS, targetList);
		assertEquals(20, targetList.size());
		assertEquals(new Integer(99), targetList.get(19));
		

		// Integer Set
		Set<Integer> targetIntSet = new HashSet<>();
		CollectionFunctions.merge(LARGEST_3_INTEGERS, SMALLEST_3_INTEGERS, targetIntSet);
		assertEquals(6, targetIntSet.size());
		
		// Person Set
		Set<Person> targetSet = new HashSet<>();
		CollectionFunctions.merge(PEOPLE_LIST, LARGEST_2_PEOPLE, targetSet);
		assertEquals(16, targetSet.size()); // Merging the same object
	}
	
	@Test
	public void testMedian(){
		assertEquals(new Integer(MEDIAN_INTEGER), CollectionFunctions.median(INTEGER_LIST));
		assertEquals(MEDIAN_PERSON, CollectionFunctions.median(PEOPLE_LIST));
	}
	
	@Test
	public void testLargest(){
		assertEquals(LARGEST_3_INTEGERS, CollectionFunctions.largest(INTEGER_LIST, 3));
		assertEquals(LARGEST_2_PEOPLE, CollectionFunctions.largest(PEOPLE_LIST, 2));
	}
	
	@Test
	public void testSmallest(){
		assertEquals(SMALLEST_3_INTEGERS, CollectionFunctions.smallest(INTEGER_LIST, 3));
		assertEquals(SMALLEST_2_PEOPLE, CollectionFunctions.smallest(PEOPLE_LIST, 2));
	}
	
	@Test
	public void testGeneric() throws InstantiationException, IllegalAccessException{
		List<Integer> int1 = new ArrayList<>(LARGEST_3_INTEGERS);
		List<Integer> int2 = new ArrayList<>(SMALLEST_3_INTEGERS);
		List<Integer> merged = CollectionFunctions.merge(int1, int2);
		assertEquals("Merged Integer List", 6, merged.size());
		
	}
}
