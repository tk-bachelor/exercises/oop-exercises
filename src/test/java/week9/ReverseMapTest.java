package week9;

import static org.junit.Assert.*;

import org.junit.Test;

import week9_generics.ReverseMap;

public class ReverseMapTest {
	
	@Test
	public void testReverseMap(){
		ReverseMap<Integer, String> map = new ReverseMap<>();
		map.put(123456, "Hans Meier");
		map.put(333999, "Clara M�ller");
		
		assertEquals("Hans Meier", map.getRight(123456));
		assertEquals(new Integer(333999), map.getLeft("Clara M�ller"));
	}

}
