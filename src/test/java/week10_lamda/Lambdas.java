package week10_lamda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

import week10_lambda.Gender;
import week10_lambda.PeopleData;
import week10_lambda.Person;

@FunctionalInterface
interface PersonFactory {
	Person createPerson(String firstName, String lastName, Gender gender, String city, int age, int salary);
}

public class Lambdas {
	private static int count = 0;

	public static void main(String[] args) {
		taskA();
		taskB();
		taskC();
		taskD();
	}

	private static void taskA() {
		List<Person> personList = new ArrayList<>();
		personList.add(new Person("Hans", "Meier", Gender.MALE, "Zrich", 34, 40000));
		personList.add(new Person("Petra", "Müller", Gender.FEMALE, "Rapperswil", 25, 60000));
		personList.add(new Person("Klaus", "Schmid", Gender.MALE, "Wetzikon", 30, 21000));
		personList.add(new Person("Claudia", "Schneider", Gender.FEMALE, "Zürich", 40, 92000));
		sort(personList);
	}

	private static void sort(List<Person> personList) {
		Collections.sort(personList, (p1, p2) -> {
			count++; // Compile error
			return p1.getLastName().compareTo(p2.getLastName());
		});
		System.out.println("Number of comparisons: " + count);
	}

	private static void taskB() {
		// Assign to function interface
		BiFunction<Integer, Integer, Integer> func = (x, y) -> x + y;
		
		int t = 1, u = 2;
		System.out.println(func.apply(t, u));
		
		// x->y-> x+y
	}

	private static void taskC() {
		String[] names = { "UPPER", "up", "DOWN", "do", "reset", "REPEAT" };
		Arrays.sort(names, String::compareToIgnoreCase);
		for (String item : names) {
			System.out.print(item + " ");
		}
		System.out.println();

		// Alternative
		String[] names2 = { "UPPER", "up", "DOWN", "do", "reset", "REPEAT" };
		Arrays.sort(names2, (n1, n2) -> n1.compareToIgnoreCase(n2));
		for (String item : names2) {
			System.out.print(item + " ");
		}
		System.out.println();
	}

	private static void taskD() {
		System.out.println(createPersonList(Person::new, 10));

		// Alternative to constructor reference
		List<Person> list = createPersonList((firstname, lastname, gender, city, age, salary) -> new Person(firstname,
				lastname, gender, city, age, salary), 10);
		System.out.println(list);
	}

	private static List<Person> createPersonList(PersonFactory factory, int amount) {
		List<Person> list = new ArrayList<>();
		for (int i = 0; i < amount; i++) {
			list.add(factory.createPerson("FirstName" + i, "LastName" + i, Gender.MALE, "Zürich", 30, 0));
		}
		return list;
	}
}
