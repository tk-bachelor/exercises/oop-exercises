package week10_lamda;

import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.List;

import org.junit.Test;

import week10_lambda.Gender;
import week10_lambda.PeopleData;
import week10_lambda.Person;
import week10_lambda.PersonFilter;

public class FilterPerson {

	@Test
	public void testFilterByMale() throws IOException {
		List<Person> people = PeopleData.read("people.csv");
		List<Person> selected = PersonFilter.search(people, this::isMale);

		for (int i = 0; i < selected.size() - 1; i++) {
			if (selected.get(i).getGender() != Gender.MALE)
				fail("Filter male people : index " + i);
		}
		System.out.println("Total male people: " + selected.size() + "/" + people.size());
		PeopleData.write("output/filteredByGenderMale.csv", selected);
	}

	@Test
	public void testFilterByCity() throws IOException {
		List<Person> people = PeopleData.read("people.csv");
		List<Person> selected = PersonFilter.search(people, person -> person.getCity().equals("Zug"));

		for (int i = 0; i < selected.size() - 1; i++) {
			if (!selected.get(i).getCity().equals("Zug"))
				fail("Filter by city : index " + i);
		}
		System.out.println("Total people from Zug: " + selected.size() + "/" + people.size());
		PeopleData.write("output/filteredByCity.csv", selected);
	}

	@Test
	public void testFilterByAge18And65() throws IOException {
		List<Person> people = PeopleData.read("people.csv");
		List<Person> selected = PersonFilter.search(people, person -> person.getAge() > 18 && person.getAge() < 65);

		for (int i = 0; i < selected.size() - 1; i++) {
			if (selected.get(i).getAge() <= 18 || selected.get(i).getAge() >= 65)
				fail("Filter by age between 18 and 65 : index " + i);
		}
		System.out.println("Total people between 18 and 65: " + selected.size() + "/" + people.size());
	}

	@Test
	public void testFilterByMaleZurich() throws IOException {
		List<Person> people = PeopleData.read("people.csv");
		List<Person> selected = PersonFilter.search(people,
				person -> person.getGender() == Gender.MALE && person.getCity().equals("Z�rich"));

		for (int i = 0; i < selected.size() - 1; i++) {
			if (selected.get(i).getGender() != Gender.MALE && selected.get(i).getCity().equals("Z�rich"))
				fail("Filter male people from zurich: index " + i);
		}
		System.out.println("Total male people from zurich: " + selected.size() + "/" + people.size());
		PeopleData.write("output/filteredByMaleZH.csv", selected);
	}

	private boolean isMale(Person p) {
		return p.getGender() == Gender.MALE;
	}

}
