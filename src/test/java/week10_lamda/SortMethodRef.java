package week10_lamda;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import week10_lambda.PeopleData;
import week10_lambda.Person;
import week10_lambda.PersonUtil;

public class SortMethodRef {

	@Test
	public void testSortByAge() throws IOException {
		List<Person> people = PeopleData.read("people.csv");
		people.sort(PersonUtil::compareByAge);

		for (int i = 0; i < people.size() - 1; i++) {
			if (people.get(i).getAge() > people.get(i + 1).getAge())
				fail("Sort by age : index " + i);
		}
		PeopleData.write("output/sortedByAge.csv", people);
	}

	@Test
	public void testSortByAgeDESC() throws IOException {
		List<Person> people = PeopleData.read("people.csv");
		people.sort(PersonUtil::compareByAgeDesc);

		for (int i = 0; i < people.size() - 1; i++) {
			if (people.get(i).getAge() < people.get(i + 1).getAge())
				fail("Sort by age desc: index " + i);
		}
		PeopleData.write("output/sortedByAgeDesc.csv", people);
	}

	@Test
	public void testSortByLastname() throws IOException {
		List<Person> people = PeopleData.read("people.csv");
		people.sort(PersonUtil::compareByLastname);

		for (int i = 0; i < people.size() - 1; i++) {
			if (people.get(i).getLastName().compareTo(people.get(i + 1).getLastName()) > 0)
				fail("Sort by lastname: index " + i);
		}
		PeopleData.write("output/sortedByLastname.csv", people);
	}

	@Test
	public void testSortByNameLengthDesc() throws IOException {
		List<Person> people = PeopleData.read("people.csv");
		people.sort(PersonUtil::compareByNameLength);
		Collections.reverse(people);

		for (int i = 0; i < people.size() - 1; i++) {
			String strP1 = people.get(i).getFirstName() + people.get(i).getLastName();
			String strP2 = people.get(i + 1).getFirstName() + people.get(i + 1).getLastName();
			if (strP1.length() < strP2.length())
				fail("Sort by name length desc: index " + i);
		}
		PeopleData.write("output/sortedByNameLengthDesc.csv", people);
	}

	@Test
	public void testSortByPlaceNameFirstname() throws IOException {
		List<Person> people = PeopleData.read("people.csv");
		people.sort(PersonUtil::compareByPlaceAndName);

		for (int i = 0; i < people.size() - 1; i++) {
			if (people.get(i).getCity().compareTo(people.get(i + 1).getCity()) > 0)
				fail("Sort by place and then name: index " + i);
			else if (people.get(i).getCity().compareTo(people.get(i + 1).getCity()) == 0) {
				if (people.get(i).getLastName().compareTo(people.get(i + 1).getLastName()) > 0)
					fail("Sort by lastname: index " + i);
				else if (people.get(i).getLastName().compareTo(people.get(i + 1).getLastName()) == 0) {
					if (people.get(i).getFirstName().compareTo(people.get(i + 1).getFirstName()) > 0)
						fail("Sort by firstname: index " + i);
				}
			}
		}
		PeopleData.write("output/sortedByPlaceAndName.csv", people);
	}

	@Test
	public void testSortByNameAndAgeDesc() throws IOException {
		List<Person> people = PeopleData.read("people.csv");
		people.sort(PersonUtil::compareByNameAndAgeDesc);

		for (int i = 0; i < people.size() - 1; i++) {
			if (people.get(i).getLastName().compareTo(people.get(i + 1).getLastName()) > 0)
				fail("Sort by lastname: index " + i);
			else if (people.get(i).getLastName().compareTo(people.get(i + 1).getLastName()) == 0) {
				if (people.get(i).getAge() < people.get(i + 1).getAge())
					fail("Sort by age desc: index " + i);
			}
		}
		PeopleData.write("output/sortedByNameAgeDesc.csv", people);
	}

}
