package week10_lamda;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import week10_lambda.PeopleData;
import week10_lambda.Person;
import week10_lambda.QueryList;

public class QueryListTester {

	@Test
	public void testQueryListAverage() throws IOException {
		QueryList<Person> list = new QueryList<>(PeopleData.read("people.csv"));
		double average = list.average(p -> p.getAge());
		System.out.println(average);
		assertEquals(44.123, average, 0.001);
	}

	@Test
	public void testLongNamer() throws IOException {
		QueryList<Person> list = new QueryList<>(PeopleData.read("people.csv"));

		Person longNamer = list.maximum(p -> p.getLastName().length());
		String longNamerStr = longNamer.getFirstName() + " " + longNamer.getLastName();
		System.out.println(longNamerStr);
		assertEquals("Holly Hildebrandt", longNamerStr);
	}

	@Test
	public void testAscendinglySorted() throws IOException {
		QueryList<Person> list = new QueryList<>(PeopleData.read("people.csv"));

		boolean menFirst = list.ascendinglySorted(p -> p.getGender());
		System.out.println("Men First :" + menFirst);
		assertEquals(false, menFirst);
	}
	
	@Test
	public void testDescendinglySorted() throws IOException {
		QueryList<Person> list = new QueryList<>(PeopleData.read("people.csv"));

		boolean womenFirst = list.descendinglySorted(p -> p.getGender());
		System.out.println("Women First :" + womenFirst);
		assertEquals(true, womenFirst);
	}
}
