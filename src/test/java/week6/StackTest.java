package week6;

import static org.junit.Assert.*;

import org.junit.Test;

import week6_1_stack.ObjectNullException;
import week6_1_stack.Stack;
import week6_1_stack.StackCapacityException;
import week6_1_stack.StackEmptyException;
import week6_1_stack.StackFullException;

public class StackTest {

	@Test(expected = StackCapacityException.class)
	public void testMinCapacity() throws StackCapacityException {
		new Stack(-3);
	}

	@Test(expected = StackCapacityException.class)
	public void testMaxCapacity() throws StackCapacityException {
		new Stack(70004);
	}

	@Test
	public void testValidStack() throws StackCapacityException {
		Stack stack = new Stack(10);
		assertNotNull(stack);
	}

	@Test
	public void testPush() throws StackCapacityException, ObjectNullException, StackFullException {
		Stack stack = new Stack(3);
		stack.push(1);
		stack.push(2);
		stack.push(3);
		System.out.println("testPush: Stack size = " + stack.size());

		assertTrue(stack.size() == 3);
	}

	@Test(expected = StackFullException.class)
	public void testStackFull() throws StackCapacityException, ObjectNullException, StackFullException {
		Stack stack = new Stack(3);
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.push(4);
	}

	@Test(expected = ObjectNullException.class)
	public void testElementNull() throws StackCapacityException, ObjectNullException, StackFullException {
		Stack stack = new Stack(3);
		stack.push(1);
		stack.push(null);
	}

	public void testPop() throws StackCapacityException, ObjectNullException, StackFullException, StackEmptyException {
		Stack stack = new Stack(3);
		stack.push(1);
		stack.push(2);
		stack.push(3);

		int three = (int) stack.pop();
		assertTrue(three == 3);

		int two = (int) stack.pop();
		assertTrue(two == 2);

		int one = (int) stack.pop();
		assertTrue(one == 1);

		assertTrue(one == 0);
	}

	@Test(expected = StackEmptyException.class)
	public void testStackEmpty()
			throws StackCapacityException, ObjectNullException, StackFullException, StackEmptyException {
		Stack stack = new Stack(3);
		stack.push(1);
		stack.push(2);
		stack.push(3);

		// stack 1,2,3
		stack.pop(); // 1, 2
		stack.pop(); // 1
		stack.pop(); // empty
		stack.pop(); // empty exception
	}
}
