package week6;

import static org.junit.Assert.*;

import org.junit.Test;

import week6_2_bank.Bank;
import week6_2_bank.BankAccount;
import week6_2_bank.TransactionException;

public class BankTransactionTest {

	@Test
	public void testValidTransaction() throws TransactionException {
		BankAccount from = new BankAccount(50, 20, 100);
		BankAccount to = new BankAccount(100, 20, 500);

		Bank bank = new Bank();
		bank.transfer(from, to, 20);

		assertTrue(from.getBalance() == 30);
		assertTrue(to.getBalance() == 120);
		System.out.println("Valid Transation: Sender =  "+from.getBalance() + " Recipient = "+ to.getBalance());
	}

	@Test(expected = TransactionException.class)
	public void testSenderLimitFailure() throws TransactionException {
		BankAccount from = new BankAccount(50, 20, 100);
		BankAccount to = new BankAccount(100, 20, 500);

		Bank bank = new Bank();
		bank.transfer(from, to, 40);
	}

	@Test(expected = TransactionException.class)
	public void testRecipientLimitFailure() throws TransactionException {
		BankAccount from = new BankAccount(50, 20, 100);
		BankAccount to = new BankAccount(100, 20, 120);

		Bank bank = new Bank();
		try {
			bank.transfer(from, to, 30);
		} catch (TransactionException ex) {
			// check sender's amount
			System.out.println("Recipient Limit Test: Sender =  "+from.getBalance() + " Recipient = "+ to.getBalance());
			assertTrue(from.getBalance()==50);
			throw ex;
		}
	}

}
