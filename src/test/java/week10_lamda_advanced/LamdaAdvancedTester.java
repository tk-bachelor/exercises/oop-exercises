package week10_lamda_advanced;

import java.io.IOException;
import java.util.List;

import week10_lambda.PeopleData;
import week10_lambda.Person;

public class LamdaAdvancedTester {

	public static void main(String[] args) throws IOException {

		List<Person> list = PeopleData.read("people.csv");
		printPredicate(list);
	}

	private static void printPredicate(List<Person> list) {
		LamdaAdvanced.printPersonWithPredicate(list, (p -> p.getAge() == 65), (p -> System.out.println(p.toString())));
	}
}
