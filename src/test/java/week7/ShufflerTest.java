package week7;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

import week7_5_shuffle.Shuffler;

public class ShufflerTest {

	private int amount = 15;
	private int amountNew = 10000; 	// good for new method

	@Test(timeout = 5000)
	public void testRandomSeries() {
		
		String series1 = Arrays.toString(new Shuffler().randomSeries(amount));
//		System.out.println("Series 1: " + series1);

		String series2 = Arrays.toString(new Shuffler().randomSeries(amount));
//		System.out.println("Series 2: " + series2);
		assertNotEquals("Two series are the same", series1, series2);
//		System.out.println();
	}

	@Test(timeout = 5000)
	public void testRandomSeriesNew() {
		String series1 = Arrays.toString(new Shuffler().randomSeriesNew(amountNew));
//		System.out.println("Series 1: " + series1);
		
		String series2 = Arrays.toString(new Shuffler().randomSeriesNew(amountNew));
//		System.out.println("Series 2: " + series2);
		
		assertNotEquals("Two series are the same", series1, series2);
//		System.out.println();
	}

}
