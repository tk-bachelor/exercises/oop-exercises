package week7;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import week7_1_banking.Bank;
import week7_1_banking.BankAccessException;
import week7_1_banking.BankAccount;
import week7_1_banking.BankLimitException;
import week7_1_banking.BankTransferException;

public class BankTest {

	private Bank bank;

	@Before
	public void setup() {
		this.bank = new Bank();
	}

	@Test
	public void testWithdraw() throws BankLimitException {
		BankAccount account = bank.openAccount();
		// balance 0
		account.withdraw(500);
		assertTrue(account.getBalance() == (-500));
	}

	@Test(expected = BankLimitException.class)
	public void testWithdrawError1() throws BankLimitException {
		BankAccount account = bank.openAccount();
		// balance 0
		account.withdraw(1005);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testWithdrawError2() throws BankLimitException {
		BankAccount account = bank.openAccount();
		// balance 0
		account.withdraw(-10);
	}

	@Test(expected = BankAccessException.class)
	public void testWithdrawError3() throws BankLimitException {
		BankAccount account = bank.openAccount();
		account.freeze();
		account.withdraw(50);
	}

	@Test
	public void testDeposit() throws BankLimitException {
		BankAccount account = bank.openAccount();
		// balance 0
		account.deposit(500);
		assertTrue(account.getBalance() == 500);
	}

	@Test(expected = BankAccessException.class)
	public void testDepositFrozen() throws BankLimitException {
		BankAccount account = bank.openAccount();
		account.freeze();
		account.deposit(50);
	}

	@Test(expected = BankLimitException.class)
	public void testDepositLimit() throws BankLimitException {
		BankAccount account = bank.openAccount();
		// balance 0
		account.deposit(Long.MAX_VALUE - 1000);
		account.deposit(1600); // big double number problem
	}

	@Test
	public void testTransfer() throws BankLimitException, BankTransferException {
		BankAccount account = bank.openAccount();
		BankAccount account2 = bank.openAccount();

		account.deposit(50);
		bank.transfer(account, account2, 40);

		assertTrue(account.getBalance() == 10);
		assertTrue(account2.getBalance() == 40);
	}

	@Test(expected = BankTransferException.class)
	public void testInvalidTransfer() throws BankLimitException, BankTransferException {
		BankAccount account = bank.openAccount();
		BankAccount account2 = bank.openAccount();

		bank.transfer(account, account2, 1050);

		assertTrue(account.getBalance() == 10);
		assertTrue(account2.getBalance() == 40);
	}

	@Test
	public void testFinalizeQuarter() throws BankLimitException {
		BankAccount posAcc = bank.openAccount();
		BankAccount posAcc2 = bank.openAccount();
		BankAccount negAcc = bank.openAccount();

		posAcc.setBalance(500);
		posAcc2.setBalance(1500);
		negAcc.setBalance(-500);

		bank.finalizeQuarter(0.25, 9.5, 5);

		// posAcc
		// Balance 500
		// debit interest = +1.25 fees = -5
		// Expected balance 496.25
		assertEquals(496.25, posAcc.getBalance(), 0.01);

		// posAcc 2
		// Balance 1500
		// debit interest = +3.75 fees = 0
		// Expected balance 1503.75
		assertEquals(1503.75, posAcc2.getBalance(), 0.01);

		// negAcc
		// Balance -500
		// credit interest = -47.5 fees = -5
		// Expected balance -552.5
		assertEquals(-552.5, negAcc.getBalance(), 0.01);
	}

	public void tearDown() {
		bank = null;
	}

}
