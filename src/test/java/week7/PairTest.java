package week7;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import week7_6_pair.OrderedPair;
import week7_6_pair.Pair;
import week7_6_pair.UnorderedPair;

public class PairTest {

	@Test
	public void testOrderedPairGetters() {
		Pair a = new OrderedPair(1, 2);
		assertEquals(1, a.getFirst());
		assertEquals(2, a.getSecond());
	}
	
	@Test
	public void testUnorderedPairGetters() {
		Pair a = new UnorderedPair(1, 2);
		assertEquals(1, a.getFirst());
		assertEquals(2, a.getSecond());
	}
	
	@Test
	public void testOrderedPairClone() throws CloneNotSupportedException {
		Pair a = new OrderedPair(1, 2);
		Pair clone = a.clone();
		assertTrue(clone instanceof OrderedPair);
		assertEquals(a.getFirst(), clone.getFirst());
		assertEquals(a.getSecond(), clone.getSecond());
	}
	
	@Test
	public void testUnorderedPairClone() throws CloneNotSupportedException {
		Pair a = new UnorderedPair(1, 2);
		Pair clone = a.clone();
		assertTrue(clone instanceof UnorderedPair);
		assertEquals(a.getFirst(), clone.getFirst());
		assertEquals(a.getSecond(), clone.getSecond());
	}
	
	@Test
	public void testOrderedPairEquals() {
		Pair a = new OrderedPair(1, 2);
		checkEquals(a, a);
		assertFalse(a.equals(null));
		Pair b = new OrderedPair(1, 2);
		checkEquals(b, a);
	}
	
	@Test
	public void testUnorderedPairEquals() {
		Pair a = new UnorderedPair(1, 2);
		checkEquals(a, a);
		assertFalse(a.equals(null));
		Pair b = new UnorderedPair(1, 2);
		checkEquals(b, a);
		Pair c = new UnorderedPair(2, 1);
		checkEquals(c, a);
	}
	
	@Test
	public void testInterPairEquals() {
		Pair a = new OrderedPair(1, 2);
		Pair b = new UnorderedPair(1, 2);
		checkDiffers(a, b);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testOrderedPairNullFirst() {
		new OrderedPair(null, 1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testOrderedPairNullSecond() {
		new OrderedPair(1, null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testUnorderedPairNullFirst() {
		new UnorderedPair(null, 1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testUnorderedPairNullSecond() {
		new UnorderedPair(1, null);
	}
	
	private void checkEquals(Object obj1, Object obj2) {
		assertTrue(obj1.equals(obj2));
		assertTrue(obj2.equals(obj1));
		assertEquals(obj1.hashCode(), obj2.hashCode());
	}
	
	private void checkDiffers(Object obj1, Object obj2) {
		assertFalse(obj1.equals(obj2));
		assertFalse(obj2.equals(obj1));
	}
}
