package testat3_test;

import java.io.IOException;
import java.util.List;

import testat3_io.ProcessingStreamHelper;
import testat3_story.StoryTeller;

public class StoryReconstructionTest {
	public static void main(String[] args) throws IOException {
		List<String> lines = ProcessingStreamHelper.readAllLines("story-input.txt");
		String story = StoryTeller.constructStory(lines);
		
		ProcessingStreamHelper.writeIntoFile("output/story.txt", story);
	}
}