package testat3_test;

import org.junit.Test;	

import junit.framework.TestCase;
import testat3_regex.DateConverter;

public class DateConverterTest extends TestCase{
	
	@Test
	public void testDateConversion1(){
		DateConverter converter = new DateConverter();
		String converted = converter.convertDateToDE("Fri 11/21/2014 3:15 PM");
		assertEquals("Fr 21.11.2014 15:15", converted);
		System.out.println(converted);
	}
	
	@Test
	public void testDateConversion2(){
		DateConverter converter = new DateConverter();
		String converted = converter.convertDateToDE("Tue 11/18/2014 8:00 AM");
		assertEquals("Di 18.11.2014 8:00", converted);
		System.out.println(converted);
	}
	
	@Test
	public void testDateConversion3(){
		DateConverter converter = new DateConverter();
		String converted = converter.convertDateToDE("Wed 1/1/2015 12:00 AM");
		assertEquals("Mi 1.1.2015 0:00", converted);
		System.out.println(converted);
	}
}
