package testat3_test;

import java.io.IOException;
import java.util.List;

import org.junit.Test;

import testat3_regex.WebLinkAnalyzer;

public class WebLinkAnalyzerTest {

	@Test
	public void testHSRLink() throws IOException {
		WebLinkAnalyzer analyzer = new WebLinkAnalyzer();
		String domain = "http://www.hsr.ch/";
		
		List<String> foundLinks = analyzer.analyseLinks(domain);
		
		// print out infos
		// 132 links found
		System.err.println(foundLinks.size() + " Link(s) found.");
		System.out.println();		
		foundLinks.forEach(link -> System.out.println(link));
		
		// Not reachable links
		System.out.println();
		System.err.println("Checking reachable urls");
		System.err.println("===================");
		analyzer.printNotReachableLinks(domain, foundLinks);
		
		// result summary
		// 1 of 132 link is not reachable
		// Not Found - http://www.hsr.ch/index.php?id=14362
	}
}
