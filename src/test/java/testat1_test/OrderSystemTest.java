package testat1_test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import testat1_model.Item;
import testat1_model.Order;
import testat1_model.ProductItem;
import testat1_model.ServiceItem;

/**
 * @author Keerthikan
 * 			
 *         Testat 1 Exercise 1 - Bestellungssystem Test
 */
public class OrderSystemTest {

	private Order order;

	@Before
	public void setup() {
		order = new Order();

		// Product
		Item product = new ProductItem("Samsung Galaxy S6 64GB", 699, 2);
		Item product2 = new ProductItem("Apple iPhone 6s", 879, 3);

		// Service
		Item service = new ServiceItem("24-Months Warranty Service", 99);
		Item service2 = new ServiceItem("Repair Without Warranty", 49.5);

		// Add Items
		order.addItem(product);
		order.addItem(product2);
		order.addItem(service);
		order.addItem(service2);
	}

	@Test
	public void testTotalItems() {
		int expected = 4;
		assertEquals("Total Items", expected, order.getItems().size());

		//order.printItems();
	}

	@Test
	public void testTotalPrice() {
		double expected = 4183.5; // 2 * 699 + 3 * 879 + 99 + 49.50;
		assertEquals("Total Price", expected, order.getTotalPrice(), 0.01);

		//order.printItems();
	}

}
