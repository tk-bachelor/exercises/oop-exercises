package testat1_test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import testat1_model.BundleItem;
import testat1_model.Item;
import testat1_model.Order;
import testat1_model.ProductItem;
import testat1_model.ServiceItem;

/**
 * @author Keerthikan
 * 
 *         Testat 1 Exercise 2 - Bundle-Position
 */
public class BundlePositionTest {

	private Order order;

	@Before
	public void setup() {
		order = new Order();

		// Products
		Item galaxyS6 = new ProductItem("Samsung Galaxy S6 64GB", 699, 1);
		Item iPhone6s = new ProductItem("Apple iPhone 6s", 879, 1);

		// Services
		Item warrantyService = new ServiceItem("24-Months Warranty Service", 99);
		Item repairService = new ServiceItem("Repair Without Warranty", 49.5);

		// Bundles
		BundleItem samsungBundle = new BundleItem("Samsung Collection", 30);
		samsungBundle.addItem(galaxyS6);
		samsungBundle.addItem(warrantyService);

		BundleItem appleBundle = new BundleItem("Apple Collection", 25);
		appleBundle.addItem(iPhone6s);
		appleBundle.addItem(warrantyService);
		appleBundle.addItem(repairService);

		// Phone Bundle
		BundleItem phoneBundle = new BundleItem("Phone Collection", 50);
		phoneBundle.addItem(samsungBundle);
		phoneBundle.addItem(appleBundle);

		// Add 2 Items
		order.addItem(galaxyS6);
		order.addItem(warrantyService);

		// Apple Bundle with 1 product and two services
		order.addItem(appleBundle);

		// Phone Bundle with two bundles
		// 1. Samsung Bundle with 1 product and 1 service
		// 2. Apple Bundle with 1 product and 2 services
		order.addItem(phoneBundle);
	}

	@Test
	public void testTotalItems() {
		int expected = 4;
		assertEquals("Total Items", expected, order.getItems().size());

//		order.printItems();
	}

	@Test
	public void testTotalPrice() {
		// Total Price
		// 1. Galaxy S6 = 699
		// 2. Warranty = 99

		// 3. Apple Bundle 25% Discount = 1027.50 * 0.75 = 770.625
		// 3.1 iPhone = 879
		// 3.2 Warranty = 99
		// 3.3 Repair = 49.50
		
		// 4. Phone Bundle 50% Discount = 1329.225 * 0.5 = 664.6125
		// 4.1. Samsung Bundle 30% Discount = (699+99) * 0.7 = 558.60
		// 4.2. Apple Bundle 25% Discount = 770.625

		double expected = 2233.24; // Exactly 2233.2375
		assertEquals("Total Price", expected, order.getTotalPrice(), 0.01);
	}
}
