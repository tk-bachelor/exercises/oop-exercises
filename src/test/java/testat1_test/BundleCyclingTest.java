package testat1_test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import testat1_model.BundleItem;
import testat1_model.Item;
import testat1_model.Order;
import testat1_model.ProductItem;
import testat1_model.ServiceItem;

/**
 * @author Keerthikan
 * 
 *         Testat 1 Exercise 3 - Bundle-Cycling
 */
public class BundleCyclingTest {

	private Order order;
	
	// Reusable Bundles
	private BundleItem samsungBundle;
	private BundleItem appleBundle;	
	private BundleItem phoneBundle;

	@Before
	public void setup() {
		order = new Order();

		// Products
		Item galaxyS6 = new ProductItem("Samsung Galaxy S6 64GB", 699, 1);
		Item iPhone6s = new ProductItem("Apple iPhone 6s", 879, 1);

		// Services
		Item warrantyService = new ServiceItem("24-Months Warranty Service", 99);
		Item repairService = new ServiceItem("Repair Without Warranty", 49.5);

		// Bundle 1 - Samsung Collection
		// Samsung Bundle with 1 product and 1 service
		samsungBundle = new BundleItem("Samsung Collection", 30);
		samsungBundle.addItem(galaxyS6);
		samsungBundle.addItem(warrantyService);

		// Bundle 2 - Apple Collection
		// Apple Bundle with 1 product and two services
		appleBundle = new BundleItem("Apple Collection", 25);
		appleBundle.addItem(iPhone6s);
		appleBundle.addItem(warrantyService);
		appleBundle.addItem(repairService);

		// Phone Bundle with Samsung and Apple Collection
		phoneBundle = new BundleItem("Phone Collection", 50);
		
		// Add 2 Items
		order.addItem(galaxyS6);
		order.addItem(warrantyService);		
	}

	/**
	 * Parent-Child Bundle References
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testDirectReference() {
		
		phoneBundle.addItem(samsungBundle);
		phoneBundle.addItem(appleBundle);
		
		// Adding phone bundle to itself
		// Exception expected
		phoneBundle.addItem(phoneBundle);
				
		order.addItem(appleBundle);
		order.addItem(samsungBundle);
		
		fail("PhoneBundle cannot be added to itself again");
	}
	
	/**
	 * Parent-Grand child Bundle References
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testIndirectReference() {
		
		phoneBundle.addItem(samsungBundle);
		phoneBundle.addItem(appleBundle);
		
		// Adding phone bundle to samsung bundle
		// Exception expected
		samsungBundle.addItem(phoneBundle);
		//order.printItems();
		fail("PhoneBundle cannot be added to samsung bundle again");
	}
	
	/**
	 * Parent-X Grand Child Bundle References
	 * @throws CloneNotSupportedException 
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testDeepIndirectReference() throws CloneNotSupportedException {
		
		// Origin
		BundleItem origin = phoneBundle.clone();
		origin.setDescription("Origin O");
		origin.addItem(appleBundle);
		origin.addItem(samsungBundle);
		
		// Child
		BundleItem child = phoneBundle.clone();
		child.setDescription("Child O->C");
		child.addItem(samsungBundle.clone());		
		origin.addItem(child);
		
		// Grand child
		BundleItem grandchild = phoneBundle.clone();
		grandchild.setDescription("Grand child O->C->GC");
		grandchild.addItem(samsungBundle.clone());
		child.addItem(grandchild);
		
		// Grand Grand child
		BundleItem grandGrandChild = phoneBundle.clone();
		grandGrandChild.setDescription("Grand grand child O->C->GC->GGC");		
		grandchild.addItem(grandGrandChild);
		
		// Origin in grand grand child
		// Exception expected
		grandGrandChild.addItem(origin);		
		
		//order.printItems();
		fail("Origin in grand grand child cannot be added");
	}
	
	/**
	 * Valid Bundle System
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testValidDeepStructure() throws CloneNotSupportedException {
		
		// Origin
		BundleItem origin = phoneBundle.clone();
		origin.setDescription("Origin O");
		origin.addItem(appleBundle);
		origin.addItem(samsungBundle);
		
		// Child
		BundleItem child = phoneBundle.clone();
		child.setDescription("Child O->C");
		child.addItem(samsungBundle.clone());		
		origin.addItem(child);
		
		// Grand child
		BundleItem grandchild = phoneBundle.clone();
		grandchild.setDescription("Grand child O->C->GC");
		grandchild.addItem(samsungBundle.clone());
		child.addItem(grandchild);
		
		// Grand Grand child
		BundleItem grandGrandChild = phoneBundle.clone();
		grandGrandChild.setDescription("Grand grand child O->C->GC->GGC");		
		grandchild.addItem(grandGrandChild);
		
		// Parallel
		phoneBundle.addItem(samsungBundle);
		grandGrandChild.addItem(phoneBundle);
		grandGrandChild.addItem(phoneBundle);
		
		// Parallel
		order.addItem(origin);
		order.addItem(origin);
		//order.printItems();
	}
}
