package testat1_test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ OrderSystemTest.class, BundlePositionTest.class, BundleCyclingTest.class })
public class Testat1_AllTests {

}
