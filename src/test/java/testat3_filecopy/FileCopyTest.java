package testat3_filecopy;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * Exercise 1 - FileCopyTest
 * 
 * @author Keerthikan
 *
 */
public class FileCopyTest {
	public static void main(String[] args) {

		try (Scanner scanner = new Scanner(System.in)) {
			// Get source path
			System.out.print("Source file path: ");
			Path sourcePath = Paths.get(scanner.nextLine());

			// Target Path
			System.out.print("Target Path: ");
			Path targetPath = Paths.get(scanner.nextLine());

			// Test
			// source: D:\Keerthikan\Downloads\test.txt
			//Files.copy(sourcePath, targetPath, StandardCopyOption.REPLACE_EXISTING);
			FileCopy.copy(sourcePath.toString(), targetPath.toString(), true);
			// success message
			System.out.println("File has been successfully copied");
		} catch (IOException e) {
			System.err.println("Failed to copy file");
			e.printStackTrace();
		}
	}
}
