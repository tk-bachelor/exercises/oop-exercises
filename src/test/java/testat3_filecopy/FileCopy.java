package testat3_filecopy;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileCopy {

	public static void copy(String source, String target, boolean isExisting) throws IOException {
		
		try (FileInputStream inputStream = new FileInputStream(source);
				FileOutputStream outputStream = new FileOutputStream(target)) {
			byte[] buffer = new byte[4096];
			int numberOfBytes;
			while ((numberOfBytes = inputStream.read(buffer)) > 0) {
				outputStream.write(buffer, 0, numberOfBytes);
			}
		}
	}
}
