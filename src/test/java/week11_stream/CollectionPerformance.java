package week11_stream;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.Ignore;
import org.junit.Test;

import week10_lambda.Gender;
import week10_lambda.PeopleData;
import week10_lambda.Person;

/**
 * 
 * @author Keerthikan.
 * 
 *         StreamPerformance: 36.331s. <br/>
 *         testNameLength 8.6s. <br>
 *         testAverageAge 5.5s. <br>
 *         testAverageAgeCity 6.7s <br>
 *         testMinMaxAge: 6.1s <br>
 *         testTop10Incom: 6.3 <br>
 */
public class CollectionPerformance {

	@Test
	public void testNameLength3() throws IOException {
		List<Person> people = PeopleData.read("large.csv");

		List<Person> output = new ArrayList();
		for (Person p : people) {
			if (p.getFirstName().length() <= 3 && p.getGender() == Gender.FEMALE) {
				if (!output.contains(p)) {
					output.add(p);
				}
			}
		}

		output.sort(Comparator.comparing(Person::getFirstName));
	}

	@Test
	public void testAverageAge() throws IOException {
		List<Person> people = PeopleData.read("large.csv");

		int totalAge = 0;
		for (Person p : people) {
			if (p.getGender() == Gender.MALE) {
				totalAge += p.getAge();
			}
		}
		if (!people.isEmpty()) {
			double avg = totalAge / people.size();
			System.out.println(avg);
		}
	}

	@Test
	public void testMinMaxAge() throws IOException {
		List<Person> people = PeopleData.read("large.csv");

		int maxAge = 0;
		for (Person p : people) {
			if (p.getCity().equals("Z�rich")) {
				maxAge = (p.getAge() > maxAge) ? p.getAge() : maxAge;
			}
		}

		if (!people.isEmpty()) {
			System.out.println(maxAge);
		}

		// minAge
		int minAge = 0;
		for (Person p : people) {
			if (p.getCity().equals("Z�rich")) {
				minAge = (p.getAge() < minAge) ? p.getAge() : minAge;
			}
		}

		if (!people.isEmpty()) {
			System.out.println(maxAge);
		}
	}

	@Test
	public void testTop10Income() throws IOException {
		List<Person> people = PeopleData.read("large.csv");

		people.sort(Comparator.comparing(Person::getSalary).reversed());
		List<Person> top10Rich = people.subList(0, 10);
	}

	@Test
	public void testAverageAgePerCity() throws IOException {
		List<Person> people = PeopleData.read("large.csv");

		Map<String, List<Integer>> output = new HashMap<>();
		for (Person p : people) {
			// Initital
			if (!output.containsKey(p.getCity())) {
				output.put(p.getCity(), new ArrayList());
			}

			// put age
			List<Integer> list = output.get(p.getCity());
			list.add(p.getAge());

			output.put(p.getCity(), list);
		}

		output.forEach((city, avg) -> {
			double total = 0;
			for(int i : avg){
				total += i;
			}
			System.out.println(String.format("%s - %f", city, (total / avg.size())));
		});
	}
}
