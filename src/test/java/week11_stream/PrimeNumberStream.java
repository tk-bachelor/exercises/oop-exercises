package week11_stream;

import java.util.stream.IntStream;

public class PrimeNumberStream {

	public static void main(String[] args) {
		//getPrimeNumbers(1000);
		getPrimeAt(1000000);
		//countPrimes(2, 1000000);
	}

	public static void getPrimeNumbers(int limit) {
		// First prime number 2
		System.out.println(2);

		// start from 3
		IntStream.iterate(3, i -> i + 2).filter(i -> isPrime(i)).limit(limit - 1).forEach(System.out::println);
	}

	public static void getPrimeAt(int position) {
		if (position == 1)
			System.out.println(2);
		else {
			// start from 3
			// 1'000'000 = 15485863
			// 30sec
			// Laptop: 30633
			long start = System.currentTimeMillis();
			IntStream.iterate(3, i -> i + 2).filter(i -> isPrime(i)).skip(position - 1).limit(1).max()
					.ifPresent(System.out::println);
			long stop = System.currentTimeMillis();
			
			System.out.println("Time elapsed:" + (stop - start));
		}
	}
	
	public static void countPrimes(int start, int end) {
		long count = IntStream.range(start, end).filter(i -> isPrime(i)).count();
		System.out.println("Total Primes: " + count);
		
		// 2 to 1'000'000
		// => 78 498
	}
	
	public static boolean isPrime(long number) {
		if (number <= 1) {
			throw new IllegalArgumentException("number must be > 1");
		}
		for (long x = 2; x <= Math.sqrt(number); x++) {
			if (number % x == 0) {
				return false;
			}
		}
		return true;
	}
}
