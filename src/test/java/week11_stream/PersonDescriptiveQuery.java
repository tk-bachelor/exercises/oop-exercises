package week11_stream;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.OptionalDouble;
import java.util.OptionalInt;
import java.util.stream.Collectors;

import org.junit.Test;

import week10_lambda.Gender;
import week10_lambda.PeopleData;
import week10_lambda.Person;

public class PersonDescriptiveQuery {

	@Test
	public void testNameLength3() throws IOException {
		List<Person> people = PeopleData.read("people.csv");

		List<Person> output = people.stream()
				.filter(p -> p.getFirstName().length() <= 3 && p.getGender() == Gender.FEMALE).map(p -> p).distinct()
				.sorted(Comparator.comparing(Person::getFirstName)).collect(Collectors.toList());
		PeopleData.write("output/streamNameLength3.csv", output);
	}

	@Test
	public void testAverageAge() throws IOException {
		List<Person> people = PeopleData.read("people.csv");

		OptionalDouble average = people.stream().filter(p -> p.getGender() == Gender.MALE).mapToInt(p -> p.getAge())
				.average();

		average.ifPresent(System.out::println);
	}

	@Test
	public void testMinMaxAge() throws IOException {
		List<Person> people = PeopleData.read("people.csv");

		OptionalInt maxAge = people.stream().filter(p -> p.getCity().equals("Z�rich")).mapToInt(p -> p.getAge()).max();

		maxAge.ifPresent(i -> System.out.println("Max age in Z�rich - " + i));

		OptionalInt minAge = people.stream().filter(p -> p.getCity().equals("Z�rich")).mapToInt(p -> p.getAge())
				.reduce((age1, age2) -> age1 < age2 ? age1 : age2);

		minAge.ifPresent(i -> System.out.println("Min age in Z�rich - " + i));
	}

	@Test
	public void testTop10Income() throws IOException {
		List<Person> people = PeopleData.read("people.csv");

		List<Person> output = people.stream().sorted(Comparator.comparing(Person::getSalary).reversed()).limit(10)
				.collect(Collectors.toList());

		PeopleData.write("output/streamTop10Income.csv", output);
	}

	@Test
	public void testAverageAgePerCity() throws IOException {
		List<Person> people = PeopleData.read("people.csv");

		Map<String, Double> output = people.stream()
				.collect(Collectors.groupingBy(Person::getCity, Collectors.averagingDouble(Person::getAge)));

		output.forEach((city, avg) -> System.out.println(String.format("%s - %f", city, avg)));
	}
}
