package week11_stream;

import java.util.Arrays;
import java.util.Random;

import week11_sort.SortUtil;

public class SortBenchmark {
	private static final int NOF_ELEMENTS = 1000*1000;
	
	public void measureQuicksort() {
		long[] array = getTestArray();
		long start = System.currentTimeMillis();
		
		SortUtil.quickSort(array, 0, array.length-1);
		
		long end = System.currentTimeMillis();
		System.out.println("Quicksort: " + (end - start) + " ms");
		checkSorted(array);
	}

	public void measureJavaSort() {
		long[] array = getTestArray();
		long start = System.currentTimeMillis();
		Arrays.sort(array);
		long end = System.currentTimeMillis();
		System.out.println("Java Arrays.sort: " + (end - start) + " ms");
		checkSorted(array);
	}
	
	public void measureTreeSetSort() {
		long[] array = getTestArray();
		long start = System.currentTimeMillis();

		SortUtil.treeSort(array);
		
		long end = System.currentTimeMillis();
		System.out.println("Quicksort: " + (end - start) + " ms");
		checkSorted(array);
	}
	
	public void measureBubbleSort() {
		long[] array = getTestArray();
		long start = System.currentTimeMillis();
		SortUtil.bubbleSort(array);
		long end = System.currentTimeMillis();
		System.out.println("Bubble sort: " + (end - start) + " ms");
		checkSorted(array);
	}

	private long[] getTestArray() {
		Random random = new Random(4711); // arbitrary but reproducible seed
		long[] array = new long[NOF_ELEMENTS];
		for (int i = 0; i < NOF_ELEMENTS; i++) {
			array[i] = random.nextLong();
		}
		return array;
	}

	private void checkSorted(long[] array) {
		for (int i = 0; i < array.length - 1; i++) {
			if (array[i] > array[i + 1]) {
				throw new AssertionError("not sorted");
			}
		}
	}

	public static void main(String[] args) {
		SortBenchmark benchmark = new SortBenchmark();
		
		// 1'000'000 = 14s
//		benchmark.measureQuicksort();
		
		// 1'000'000 = 117ms
//		benchmark.measureJavaSort();
		
		benchmark.measureTreeSetSort();
		
		// 1000 entries = 5s
//		benchmark.measureBubbleSort();
		
	}
}
