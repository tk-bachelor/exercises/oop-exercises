package week11_stream;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.OptionalDouble;
import java.util.OptionalInt;
import java.util.stream.Collectors;

import org.junit.Test;

import week10_lambda.Gender;
import week10_lambda.PeopleData;
import week10_lambda.Person;

/**
 * 
 * @author Keerthikan.
 * 
 *         StreamPerformance: 36.331s. <br/>
 *         testNameLength 8.5s. <br>
 *         testAverageAge 5.4s. <br>
 *         testAverageAgeCity 6.5s <br>
 *         testMinMaxAge: 6.2s <br>
 *         testTop10Incom: 6.228s <br> 
 */
public class ParallelStreamPerformance {

	@Test
	public void testNameLength3() throws IOException {
		List<Person> people = PeopleData.read("large.csv");

		List<Person> output = people.parallelStream()
				.filter(p -> p.getFirstName().length() <= 3 && p.getGender() == Gender.FEMALE).map(p -> p).distinct()
				.sorted(Comparator.comparing(Person::getFirstName)).collect(Collectors.toList());
	}

	@Test
	public void testAverageAge() throws IOException {
		List<Person> people = PeopleData.read("large.csv");

		OptionalDouble average = people.stream().filter(p -> p.getGender() == Gender.MALE).mapToInt(p -> p.getAge())
				.average();

		average.ifPresent(System.out::println);
	}

	@Test
	public void testMinMaxAge() throws IOException {
		List<Person> people = PeopleData.read("large.csv");
		OptionalInt maxAge = people.parallelStream().filter(p -> p.getCity().equals("Z�rich")).mapToInt(p -> p.getAge()).max();

		maxAge.ifPresent(i -> System.out.println("Max age in Z�rich - " + i));

		OptionalInt minAge = people.stream().filter(p -> p.getCity().equals("Z�rich")).mapToInt(p -> p.getAge())
				.reduce((age1, age2) -> age1 < age2 ? age1 : age2);

		minAge.ifPresent(i -> System.out.println("Min age in Z�rich - " + i));
	}

	@Test
	public void testTop10Income() throws IOException {
		List<Person> people = PeopleData.read("large.csv");

		List<Person> output = people.parallelStream().sorted(Comparator.comparing(Person::getSalary).reversed()).limit(10)
				.collect(Collectors.toList());
	}

	@Test
	public void testAverageAgePerCity() throws IOException {
		List<Person> people = PeopleData.read("large.csv");

		Map<String, Double> output = people.parallelStream()
				.collect(Collectors.groupingBy(Person::getCity, Collectors.averagingDouble(Person::getAge)));

		output.forEach((city, avg) -> System.out.println(String.format("%s - %f", city, avg)));
	}
}
