package week11_stream;

import java.util.stream.Stream;

import org.junit.Test;

public class StreamUtilTest {

	@Test
	public void testZip() {
		Stream<Integer> first = Stream.of(1, 2, 3);
		Stream<Character> second = Stream.of('x', 'y', 'z');

		Stream<Pair<Integer, Character>> zipped = StreamUtil.zip(first, second);
		zipped.forEach(System.out::println);
	}
	
	@Test
	public void testUnzip() {
		Pair<Integer, Character> one = new Pair<Integer, Character>(1, 'x');
		Pair<Integer, Character> two = new Pair<Integer, Character>(2, 'y');
		Pair<Integer, Character> three = new Pair<Integer, Character>(3, 'z');

		Stream<Pair<Integer, Character>> zipped = Stream.of(one, two, three);
		Pair<Stream<Integer>, Stream<Character>> unzipped = StreamUtil.unzip(zipped);
		
		Stream<Integer> intStream = unzipped.getFirst();
		Stream<Character> charStream = unzipped.getSecond();
		
		System.out.println("Left Stream");
		intStream.forEach(System.out::println);
		
		System.out.println("Right Stream");
		charStream.forEach(System.out::println);
	}
	
	@Test
	public void testGroup() {
		Pair<Integer, Character> one = new Pair<Integer, Character>(1, 'x');
		Pair<Integer, Character> two = new Pair<Integer, Character>(1, 'y');
		Pair<Integer, Character> three = new Pair<Integer, Character>(2, 'z');

		Stream<Pair<Integer, Character>> zipped = Stream.of(one, two, three);
		Stream<Pair<Integer, Stream<Character>>> grouped = StreamUtil.group(zipped);
		
		System.out.println("Grouped Stream");
		grouped.forEach( p -> {
			System.out.print(p.getFirst() + " -> ");
			p.getSecond().forEach(System.out::print);
			System.out.println();
		});
	}
}
