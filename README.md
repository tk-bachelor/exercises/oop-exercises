# OOP_Exercises
HSR OOP Exercises

## Week 1 (15-16. Sep 15) - Procedural Programming
- Exercise 1: Double Faculty
- Exercise 2: Prime Number
- Exercise 3: Sieb des Eratosthenes
- Exercise 4: Statistics
- Exercise 5: Image Processing
- Exercise 6: Self-Study

## Week 2 (22-23. Sep 15) - Methods and Recursion
- Exercise 1: Array Functions
- Exercise 2: Recursive Double Faculty
- Exercise 3: Recursive Sum
- Exercise 4: Labyrinth
- Exercise 5: Word Generator
- Exercise 6-8: Type Conversion, Numeric calculations, Overloading
- Exercise 9: Chess Knight
- Exercise 10: Tower of Hanoi

## Week 3 (29-30. Sep 15) - OOP
- Exercise 1-4: Bank System (Bank Account, Customer and Manager)
- Exercise 5-7: Rectangle, Train

## Week 4 (06-07. Oct 15) - Testat 1: Bestellungssystem
- Bestellungssystem
- Bundle-Position
- Zyklen-Vermeidung

![Class Diagram](https://github.com/tk-codes/OOP_Exercises/blob/master/doc/Testat1Model.png)

## Week 5 (14-15. Oct 15) - Interfaces
- Interfaces of Graphic Items
- Visualization of Graphics
- Interfaces Redesign
- Default Methods
- Constant Colors (Final Class with final static fields)
- Fix Colors (Enum)
- Multiple Inheritances

![Interface Design](https://github.com/tk-codes/OOP_Exercises/blob/master/doc/Week5Interfaces.png)

## Week 6 (20-21. Oct 15) - Exception and packages
- Stack Exceptions
- Bank Transactions
- Exceptions Theory

## Week 7 (27-28. Oct 15) - JUnit Testing
- Bank Unit Testing, TDD
- Test & Fix (Shuffle, Pair)
- Equals

## Week 8 (03-04. Nov 15) - Testat 2 - Collections (Study Plan)
- Study Plan (Modules with prerequisites)
- Study Plan (Modules with Successors) - Optimized

![Class Diagram](https://github.com/tk-codes/OOP_Exercises/blob/master/doc/Testat2Model.png) 

## Week 9 (10-11. Nov 15) - Generics
- Extended Collection Helper Functions
- Generics Creation
- ReverseMap
- Generic Pair

## Week 10 (17-18. Nov 15) - Lambdas
- Sorting
- Filter
- Query List
- Lamda-Advanced


## Week 11 (24-25. Nov 15) - Stream API
- Descriptive Queries
- Stream API Performances
- Primary Numbers
- Zip and Groups
- Sort Performance

## Week 12 (01-02. Dec 15) - Testat 3 - Input/Output Streams
- FileCopy
- Story-Reconstruction
- Contact Management
- Date Converter (US - DE)
- Weblink-Analyzer

## Week 13 (08-09. Dec 15) - Reflection, Garbage Collection
- UML Tool
- Annotation
- Garbage Collection